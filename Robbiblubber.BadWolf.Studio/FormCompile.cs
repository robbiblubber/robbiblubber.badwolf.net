﻿using System;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class implements the compile window.</summary>
    internal partial class FormCompile: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Levels.</summary>
        private int[] _Levels;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance </summary>
        public FormCompile(Project project, IItem item)
        {
            InitializeComponent();
            
            Ddp ddp = Ddp.Load(PathOp.UserConfigurationPath + @"\compiler.config");
            _CboTarget.Text = ddp.GetString("out", PathOp.ApplicationDirectory + @"\out");
            _Levels = ddp.GetIntegerArray("level", new int[] { 2, 3, 4, 5 });

            _Project = project;
            if(item == null) { item = project; }

            foreach(IItemController i in ControllerManager.Controllers.Values)
            {
                _IlistCompile.Images.Add(i.ItemImageKey, i.ItemImage);
            }

            _CboSource.Items.Add(new RichComboItem(" " + _Project.Name, "project", _Project));

            foreach(Installer i in _Project.Installers)
            {
                _CboSource.Items.Add(new RichComboItem(" " + i.Name, "installer", i));
                foreach(IItem k in i.Items) { _CboSource.Items.Add(new RichComboItem(" " + k.Name, k.GetController().ItemImageKey, k)); }
            }

            foreach(Package i in _Project.Packages)
            {
                _CboSource.Items.Add(new RichComboItem(" " + i.Name, "package", i));
                foreach(IItem k in i.Items) { _CboSource.Items.Add(new RichComboItem(" " + k.Name, k.GetController().ItemImageKey, k)); }
            }

            foreach(RichComboItem i in _CboSource.Items)
            {
                if(i.Tag == item) { _CboSource.SelectedItem = i; break; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog d = new FolderBrowserDialog();

            string dir = _CboTarget.Text;
            while(!Directory.Exists(dir)) 
            { 
                dir = Path.GetDirectoryName(dir);
                if(!dir.Contains(@"\")) break;
            }
            d.SelectedPath = dir;

            if(d.ShowDialog() == DialogResult.OK) { _CboTarget.Text = d.SelectedPath; }
        }


        /// <summary>Form closing.</summary>
        private void FormCompile_FormClosing(object sender, FormClosingEventArgs e)
        {
            DdpFile ddp = Ddp.Load(PathOp.UserConfigurationPath + @"\compiler.config");
            ddp.SetString("out", _CboTarget.Text);
            ddp.SetIntegerArray("level", _Levels);
            ddp.Save();
        }


        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "Options" click.</summary>
        private void _ButtonOptions_Click(object sender, EventArgs e)
        {
            FormCompileOptions f = new FormCompileOptions(_Levels);
            if(f.ShowDialog() == DialogResult.OK)
            {
                _Levels = f.Levels;
            }
        }


        /// <summary>Button "Compile" click.</summary>
        private void _ButtonCompile_Click(object sender, EventArgs e)
        {
            _ListOut.Items.Clear();
            Compiler c = new Compiler((IItem) ((RichComboItem) _CboSource.SelectedItem).Tag, _CboTarget.Text);
            c.Feedback += _Feedback;
            c.Compile();
            c.Feedback -= _Feedback;
        }


        /// <summary>Feedback event handler.</summary>
        private void _Feedback(object sender, FeedbackEventArgs e)
        {
            if(!_Levels.Contains((int) e.Level)) return;

            _ListOut.Items.Add(e.Message, (int) e.Level);
        }
    }
}
