﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;
using Robbiblubber.Util.Library;

namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class implements the new project window.</summary>
    public partial class FormNewObject: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Overwrite allowed flag.</summary>
        private bool _AcceptOverwrite = false;

        /// <summary>Gets if project and file name are identical.</summary>
        private bool _IdenticalFileName = true;

        /// <summary>Object type.</summary>
        private NewObjectType _Type;

        /// <summary>Object extension.</summary>
        private string _Extension;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="type">Object type.</param>
        /// <param name="project">Project.</param>
        public FormNewObject(NewObjectType type, Project project = null)
        {
            InitializeComponent();
            
            _Project = project;
            switch(_Type = type)
            {
                case NewObjectType.PROJECT:
                    Text = "New Project";
                    _Extension = ".bwprj"; 
                    break;
                case NewObjectType.PACKAGE:
                    Text = "New Package";
                    _Extension = ".bwpkg";
                    Icon = Icon.FromHandle(Properties.Resources.package.GetHicon());
                    break;
                case NewObjectType.INSTALLER:
                    Text = "New Installer";
                    _Extension = ".bwnst";
                    Icon = Icon.FromHandle(Properties.Resources.installer.GetHicon());
                    break;
            }

            _ComboFile.Text = (CommonStandards.WorkingDirectory + @"\Unnamed" + _Extension);
            _TextName.Text = "Unnamed";
            _Active = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object name.</summary>
        public string ObjectName
        {
            get { return _TextName.Text; }
        }


        /// <summary>Gets the object ID.</summary>
        public string ID
        {
            get { return _TextID.Text; }
        }


        /// <summary>Gets the object description.</summary>
        public string Description
        {
            get { return _TextDescription.Text; }
        }


        /// <summary>Gets the object file name.</summary>
        public string FileName
        {
            get 
            {
                if(Path.GetFileName(_ComboFile.Text).Contains('.')) { return _ComboFile.Text; }
                return _ComboFile.Text + _Extension;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            switch(_Type)
            {
                case NewObjectType.PROJECT:
                    d.Filter = "Packager Projects (*.bwprj)|*.bwprj|All Files|*.*"; break;
                case NewObjectType.PACKAGE:
                    d.Filter = "Packager Packages (*.bwpkg)|*.bwpkg|All Files|*.*"; break;
                case NewObjectType.INSTALLER:
                    d.Filter = "Packager Installers (*.bwnst)|*.bwnst|All Files|*.*"; break;
            }
            try
            {
                d.InitialDirectory = Path.GetDirectoryName(_ComboFile.Text);
            }
            catch(Exception) {}

            if(d.ShowDialog() == DialogResult.OK) { _ComboFile.Text = d.FileName; _AcceptOverwrite = true; }
        }


        /// <summary>Name or file name changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            if(!_Active) return;

            if((sender == _TextName) && _IdenticalFileName)
            {
                _ComboFile.Text = (Path.GetDirectoryName(_ComboFile.Text) + '\\' + _TextName.Text + _Extension);
            }
            
            try
            {
                _IdenticalFileName = (_ComboFile.Text.ToLower() == (Path.GetDirectoryName(_ComboFile.Text) + '\\' + _TextName.Text + _Extension).ToLower());
            } 
            catch(Exception) { _IdenticalFileName = false; }

            try
            {
                if(string.IsNullOrWhiteSpace(_ComboFile.Text))
                {
                    _LabelFileError.Visible = true;
                    _ToolTip.SetToolTip(_LabelFileError, "File name is empty");
                }
                else if((Directory.Exists(_ComboFile.Text)) || (!Directory.Exists(Path.GetDirectoryName(_ComboFile.Text))))
                {
                    _LabelFileError.Visible = true;
                    _ToolTip.SetToolTip(_LabelFileError, "Invalid file location");
                }
                else if(!FileOp.ValidFileName(_ComboFile.Text))
                {
                    _LabelFileError.Visible = true;
                    _ToolTip.SetToolTip(_LabelFileError, "Invalid file name");
                }
                else { _LabelFileError.Visible = false; }
            }
            catch(Exception)
            {
                _LabelFileError.Visible = true;
                _ToolTip.SetToolTip(_LabelFileError, "Invalid file name");
            }

            if(sender == _TextID)
            {
                switch(Project.CheckID(_TextID.Text.Trim(), _Project))
                {
                    case -1:
                        _LabelIdError.Visible = true;
                        _ToolTip.SetToolTip(_LabelIdError, "ID is empty");
                        break;
                    case -2:
                        _LabelIdError.Visible = true;
                        _ToolTip.SetToolTip(_LabelIdError, "ID is invalid");
                        break;
                    case -3:
                        _LabelIdError.Visible = true;
                        _ToolTip.SetToolTip(_LabelIdError, "ID is ambiguous");
                        break;
                    default:
                        _LabelIdError.Visible = false; break;
                }
            }

            if(string.IsNullOrWhiteSpace(_TextName.Text))
            {
                _LabelNameError.Visible = true;
                _ToolTip.SetToolTip(_LabelNameError, "Name is empty");
            }
            else { _LabelNameError.Visible = false; }

            _ButtonOK.Enabled = !(_LabelNameError.Visible || _LabelIdError.Visible || _LabelFileError.Visible);
            if(sender == _ComboFile) { _AcceptOverwrite = false; }
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            if((!_AcceptOverwrite) && File.Exists(_ComboFile.Text))
            {
                string caption = null;
                switch(_Type)
                {
                    case NewObjectType.PROJECT:   caption = "New Project"; break;
                    case NewObjectType.PACKAGE:   caption = "New Package"; break;
                    case NewObjectType.INSTALLER: caption = "New Installer"; break;
                }
                if(MessageBox.Show("The file already exists. Do you want to overwrite it?", caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
