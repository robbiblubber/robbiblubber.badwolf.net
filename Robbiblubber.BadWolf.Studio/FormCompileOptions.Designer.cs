﻿namespace Robbiblubber.BadWolf.Packager
{
    partial class FormCompileOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCompileOptions));
            this._CheckTrace = new System.Windows.Forms.CheckBox();
            this._CheckDebug = new System.Windows.Forms.CheckBox();
            this._CheckInformation = new System.Windows.Forms.CheckBox();
            this._CheckWarning = new System.Windows.Forms.CheckBox();
            this._CheckError = new System.Windows.Forms.CheckBox();
            this._CheckCritical = new System.Windows.Forms.CheckBox();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _CheckTrace
            // 
            this._CheckTrace.AutoSize = true;
            this._CheckTrace.Location = new System.Drawing.Point(39, 31);
            this._CheckTrace.Name = "_CheckTrace";
            this._CheckTrace.Size = new System.Drawing.Size(62, 21);
            this._CheckTrace.TabIndex = 1;
            this._CheckTrace.Text = " &Trace";
            this._CheckTrace.UseVisualStyleBackColor = true;
            // 
            // _CheckDebug
            // 
            this._CheckDebug.AutoSize = true;
            this._CheckDebug.Location = new System.Drawing.Point(39, 58);
            this._CheckDebug.Name = "_CheckDebug";
            this._CheckDebug.Size = new System.Drawing.Size(70, 21);
            this._CheckDebug.TabIndex = 2;
            this._CheckDebug.Text = " &Debug";
            this._CheckDebug.UseVisualStyleBackColor = true;
            // 
            // _CheckInformation
            // 
            this._CheckInformation.AutoSize = true;
            this._CheckInformation.Location = new System.Drawing.Point(39, 85);
            this._CheckInformation.Name = "_CheckInformation";
            this._CheckInformation.Size = new System.Drawing.Size(98, 21);
            this._CheckInformation.TabIndex = 3;
            this._CheckInformation.Text = " &Information";
            this._CheckInformation.UseVisualStyleBackColor = true;
            // 
            // _CheckWarning
            // 
            this._CheckWarning.AutoSize = true;
            this._CheckWarning.Location = new System.Drawing.Point(194, 31);
            this._CheckWarning.Name = "_CheckWarning";
            this._CheckWarning.Size = new System.Drawing.Size(79, 21);
            this._CheckWarning.TabIndex = 4;
            this._CheckWarning.Text = " &Warning";
            this._CheckWarning.UseVisualStyleBackColor = true;
            // 
            // _CheckError
            // 
            this._CheckError.AutoSize = true;
            this._CheckError.Location = new System.Drawing.Point(194, 58);
            this._CheckError.Name = "_CheckError";
            this._CheckError.Size = new System.Drawing.Size(61, 21);
            this._CheckError.TabIndex = 5;
            this._CheckError.Text = " &Error";
            this._CheckError.UseVisualStyleBackColor = true;
            // 
            // _CheckCritical
            // 
            this._CheckCritical.AutoSize = true;
            this._CheckCritical.Location = new System.Drawing.Point(194, 85);
            this._CheckCritical.Name = "_CheckCritical";
            this._CheckCritical.Size = new System.Drawing.Size(70, 21);
            this._CheckCritical.TabIndex = 6;
            this._CheckCritical.Text = " Critical";
            this._CheckCritical.UseVisualStyleBackColor = true;
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(194, 137);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(141, 29);
            this._ButtonCancel.TabIndex = 8;
            this._ButtonCancel.Tag = "";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(47, 137);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(141, 29);
            this._ButtonOK.TabIndex = 7;
            this._ButtonOK.Tag = "";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            // 
            // FormCompileOptions
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(379, 201);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._CheckCritical);
            this.Controls.Add(this._CheckError);
            this.Controls.Add(this._CheckWarning);
            this.Controls.Add(this._CheckInformation);
            this.Controls.Add(this._CheckDebug);
            this.Controls.Add(this._CheckTrace);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCompileOptions";
            this.Text = "Compile Options";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _CheckTrace;
        private System.Windows.Forms.CheckBox _CheckDebug;
        private System.Windows.Forms.CheckBox _CheckInformation;
        private System.Windows.Forms.CheckBox _CheckWarning;
        private System.Windows.Forms.CheckBox _CheckError;
        private System.Windows.Forms.CheckBox _CheckCritical;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
    }
}