﻿namespace Robbiblubber.BadWolf.Packager
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNewProject = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpenProject = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveProject = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCloseProject = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuAddToProject = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddInstaller = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuAddFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddToInstaller = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddToPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMove = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCompile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBuild = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._SplitMail = new System.Windows.Forms.SplitContainer();
            this._TreeMain = new System.Windows.Forms.TreeView();
            this._CmenuTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuBuild = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuAddToProject = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddInstaller = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuAddFile = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddToInstaller = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddToPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMove = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._IlistMain = new System.Windows.Forms.ImageList(this.components);
            this._BasicItemCtrl = new Robbiblubber.BadWolf.Struct.BasicItemControl();
            this._MenuAddToSteps = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddToPackages = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddToSteps = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddToPackages = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._SplitMail)).BeginInit();
            this._SplitMail.Panel1.SuspendLayout();
            this._SplitMail.Panel2.SuspendLayout();
            this._SplitMail.SuspendLayout();
            this._CmenuTree.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuView,
            this._MenuCompile,
            this._MenuTools,
            this._MenuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1050, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "_MenuMain";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNewProject,
            this._MenuOpenProject,
            this._MenuSaveProject,
            this._MenuFileBlank0,
            this._MenuCloseProject,
            this._MenuFileBlank1,
            this._MenuAddToProject,
            this._MenuAddToInstaller,
            this._MenuAddToPackage,
            this._MenuAddToSteps,
            this._MenuAddToPackages,
            this._MenuMove,
            this._MenuRemove,
            this._MenuDelete,
            this._MenuFileBlank2,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 20);
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuNewProject
            // 
            this._MenuNewProject.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNewProject.Image")));
            this._MenuNewProject.Name = "_MenuNewProject";
            this._MenuNewProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuNewProject.Size = new System.Drawing.Size(195, 22);
            this._MenuNewProject.Text = "&New Project...";
            this._MenuNewProject.Click += new System.EventHandler(this._MenuNewProject_Click);
            // 
            // _MenuOpenProject
            // 
            this._MenuOpenProject.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpenProject.Image")));
            this._MenuOpenProject.Name = "_MenuOpenProject";
            this._MenuOpenProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpenProject.Size = new System.Drawing.Size(195, 22);
            this._MenuOpenProject.Text = "&Open Project...";
            this._MenuOpenProject.Click += new System.EventHandler(this._MenuOpenProject_Click);
            // 
            // _MenuSaveProject
            // 
            this._MenuSaveProject.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSaveProject.Image")));
            this._MenuSaveProject.Name = "_MenuSaveProject";
            this._MenuSaveProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSaveProject.Size = new System.Drawing.Size(195, 22);
            this._MenuSaveProject.Text = "&Save";
            this._MenuSaveProject.Click += new System.EventHandler(this._MenuSaveProject_Click);
            // 
            // _MenuFileBlank0
            // 
            this._MenuFileBlank0.Name = "_MenuFileBlank0";
            this._MenuFileBlank0.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuCloseProject
            // 
            this._MenuCloseProject.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCloseProject.Image")));
            this._MenuCloseProject.Name = "_MenuCloseProject";
            this._MenuCloseProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this._MenuCloseProject.Size = new System.Drawing.Size(195, 22);
            this._MenuCloseProject.Text = "&Close";
            this._MenuCloseProject.Click += new System.EventHandler(this._MenuCloseProject_Click);
            // 
            // _MenuFileBlank1
            // 
            this._MenuFileBlank1.Name = "_MenuFileBlank1";
            this._MenuFileBlank1.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuAddToProject
            // 
            this._MenuAddToProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuAddInstaller,
            this._MenuAddPackage,
            this._MenuAddBlank0,
            this._MenuAddFile});
            this._MenuAddToProject.Name = "_MenuAddToProject";
            this._MenuAddToProject.Size = new System.Drawing.Size(195, 22);
            this._MenuAddToProject.Text = "&Add";
            // 
            // _MenuAddInstaller
            // 
            this._MenuAddInstaller.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddInstaller.Image")));
            this._MenuAddInstaller.Name = "_MenuAddInstaller";
            this._MenuAddInstaller.Size = new System.Drawing.Size(145, 22);
            this._MenuAddInstaller.Text = "&Installer...";
            this._MenuAddInstaller.Click += new System.EventHandler(this._MenuAddInstaller_Click);
            // 
            // _MenuAddPackage
            // 
            this._MenuAddPackage.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddPackage.Image")));
            this._MenuAddPackage.Name = "_MenuAddPackage";
            this._MenuAddPackage.Size = new System.Drawing.Size(145, 22);
            this._MenuAddPackage.Text = "&Package...";
            this._MenuAddPackage.Click += new System.EventHandler(this._MenuAddPackage_Click);
            // 
            // _MenuAddBlank0
            // 
            this._MenuAddBlank0.Name = "_MenuAddBlank0";
            this._MenuAddBlank0.Size = new System.Drawing.Size(142, 6);
            // 
            // _MenuAddFile
            // 
            this._MenuAddFile.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddFile.Image")));
            this._MenuAddFile.Name = "_MenuAddFile";
            this._MenuAddFile.Size = new System.Drawing.Size(145, 22);
            this._MenuAddFile.Text = "&Existing File...";
            this._MenuAddFile.Click += new System.EventHandler(this._MenuAddFile_Click);
            // 
            // _MenuAddToInstaller
            // 
            this._MenuAddToInstaller.Name = "_MenuAddToInstaller";
            this._MenuAddToInstaller.Size = new System.Drawing.Size(195, 22);
            this._MenuAddToInstaller.Text = "Add";
            // 
            // _MenuAddToPackage
            // 
            this._MenuAddToPackage.Name = "_MenuAddToPackage";
            this._MenuAddToPackage.Size = new System.Drawing.Size(195, 22);
            this._MenuAddToPackage.Text = "Add";
            // 
            // _MenuMove
            // 
            this._MenuMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuMoveUp,
            this._MenuMoveDown});
            this._MenuMove.Name = "_MenuMove";
            this._MenuMove.Size = new System.Drawing.Size(195, 22);
            this._MenuMove.Text = "Move";
            this._MenuMove.DropDownOpening += new System.EventHandler(this._MenuMove_DropDownOpening);
            // 
            // _MenuMoveUp
            // 
            this._MenuMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuMoveUp.Image")));
            this._MenuMoveUp.Name = "_MenuMoveUp";
            this._MenuMoveUp.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Up)));
            this._MenuMoveUp.Size = new System.Drawing.Size(203, 22);
            this._MenuMoveUp.Text = "Move &Up";
            this._MenuMoveUp.Click += new System.EventHandler(this._MenuMoveUp_Click);
            // 
            // _MenuMoveDown
            // 
            this._MenuMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("_MenuMoveDown.Image")));
            this._MenuMoveDown.Name = "_MenuMoveDown";
            this._MenuMoveDown.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Down)));
            this._MenuMoveDown.Size = new System.Drawing.Size(203, 22);
            this._MenuMoveDown.Text = "Move &Down";
            this._MenuMoveDown.Click += new System.EventHandler(this._MenuMoveDown_Click);
            // 
            // _MenuRemove
            // 
            this._MenuRemove.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRemove.Image")));
            this._MenuRemove.Name = "_MenuRemove";
            this._MenuRemove.Size = new System.Drawing.Size(195, 22);
            this._MenuRemove.Text = "&Remove";
            this._MenuRemove.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _MenuDelete
            // 
            this._MenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDelete.Image")));
            this._MenuDelete.Name = "_MenuDelete";
            this._MenuDelete.Size = new System.Drawing.Size(195, 22);
            this._MenuDelete.Text = "&Delete...";
            this._MenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _MenuFileBlank2
            // 
            this._MenuFileBlank2.Name = "_MenuFileBlank2";
            this._MenuFileBlank2.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(195, 22);
            this._MenuRecent.Text = "Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._MenuExit.Size = new System.Drawing.Size(195, 22);
            this._MenuExit.Text = "&Exit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 20);
            this._MenuEdit.Text = "&Edit";
            // 
            // _MenuView
            // 
            this._MenuView.Name = "_MenuView";
            this._MenuView.Size = new System.Drawing.Size(44, 20);
            this._MenuView.Text = "&View";
            // 
            // _MenuCompile
            // 
            this._MenuCompile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuBuild});
            this._MenuCompile.Name = "_MenuCompile";
            this._MenuCompile.Size = new System.Drawing.Size(64, 20);
            this._MenuCompile.Text = "&Compile";
            // 
            // _MenuBuild
            // 
            this._MenuBuild.Image = ((System.Drawing.Image)(resources.GetObject("_MenuBuild.Image")));
            this._MenuBuild.Name = "_MenuBuild";
            this._MenuBuild.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._MenuBuild.Size = new System.Drawing.Size(129, 22);
            this._MenuBuild.Text = "Build...";
            this._MenuBuild.Click += new System.EventHandler(this._MenuBuild_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(46, 20);
            this._MenuTools.Text = "&Tools";
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(159, 22);
            this._MenuShowHelp.Text = "Show &Help...";
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(159, 22);
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _SplitMail
            // 
            this._SplitMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitMail.Location = new System.Drawing.Point(0, 24);
            this._SplitMail.Name = "_SplitMail";
            // 
            // _SplitMail.Panel1
            // 
            this._SplitMail.Panel1.Controls.Add(this._TreeMain);
            // 
            // _SplitMail.Panel2
            // 
            this._SplitMail.Panel2.Controls.Add(this._BasicItemCtrl);
            this._SplitMail.Size = new System.Drawing.Size(1050, 564);
            this._SplitMail.SplitterDistance = 360;
            this._SplitMail.TabIndex = 0;
            // 
            // _TreeMain
            // 
            this._TreeMain.ContextMenuStrip = this._CmenuTree;
            this._TreeMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TreeMain.ImageIndex = 0;
            this._TreeMain.ImageList = this._IlistMain;
            this._TreeMain.Location = new System.Drawing.Point(0, 0);
            this._TreeMain.Name = "_TreeMain";
            this._TreeMain.SelectedImageIndex = 0;
            this._TreeMain.Size = new System.Drawing.Size(360, 564);
            this._TreeMain.TabIndex = 0;
            this._TreeMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._TreeMain_AfterSelect);
            this._TreeMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this._TreeMain_MouseDown);
            // 
            // _CmenuTree
            // 
            this._CmenuTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuBuild,
            this._CmenuClose,
            this._CmenuTreeBlank0,
            this._CmenuAddToProject,
            this._CmenuAddToInstaller,
            this._CmenuAddToPackage,
            this._CmenuAddToSteps,
            this._CmenuAddToPackages,
            this._CmenuMove,
            this._CmenuRemove,
            this._CmenuDelete});
            this._CmenuTree.Name = "_CmenuTree";
            this._CmenuTree.Size = new System.Drawing.Size(118, 230);
            this._CmenuTree.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuTree_Opening);
            // 
            // _CmenuBuild
            // 
            this._CmenuBuild.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuBuild.Image")));
            this._CmenuBuild.Name = "_CmenuBuild";
            this._CmenuBuild.Size = new System.Drawing.Size(117, 22);
            this._CmenuBuild.Text = "Build";
            // 
            // _CmenuClose
            // 
            this._CmenuClose.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuClose.Image")));
            this._CmenuClose.Name = "_CmenuClose";
            this._CmenuClose.Size = new System.Drawing.Size(117, 22);
            this._CmenuClose.Text = "Close";
            this._CmenuClose.Click += new System.EventHandler(this._MenuCloseProject_Click);
            // 
            // _CmenuTreeBlank0
            // 
            this._CmenuTreeBlank0.Name = "_CmenuTreeBlank0";
            this._CmenuTreeBlank0.Size = new System.Drawing.Size(114, 6);
            // 
            // _CmenuAddToProject
            // 
            this._CmenuAddToProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuAddInstaller,
            this._CmenuAddPackage,
            this._CmenuAddBlank0,
            this._CmenuAddFile});
            this._CmenuAddToProject.Name = "_CmenuAddToProject";
            this._CmenuAddToProject.Size = new System.Drawing.Size(117, 22);
            this._CmenuAddToProject.Text = "Add";
            // 
            // _CmenuAddInstaller
            // 
            this._CmenuAddInstaller.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddInstaller.Image")));
            this._CmenuAddInstaller.Name = "_CmenuAddInstaller";
            this._CmenuAddInstaller.Size = new System.Drawing.Size(145, 22);
            this._CmenuAddInstaller.Text = "Installer...";
            this._CmenuAddInstaller.Click += new System.EventHandler(this._MenuAddInstaller_Click);
            // 
            // _CmenuAddPackage
            // 
            this._CmenuAddPackage.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddPackage.Image")));
            this._CmenuAddPackage.Name = "_CmenuAddPackage";
            this._CmenuAddPackage.Size = new System.Drawing.Size(145, 22);
            this._CmenuAddPackage.Text = "Package...";
            this._CmenuAddPackage.Click += new System.EventHandler(this._MenuAddPackage_Click);
            // 
            // _CmenuAddBlank0
            // 
            this._CmenuAddBlank0.Name = "_CmenuAddBlank0";
            this._CmenuAddBlank0.Size = new System.Drawing.Size(142, 6);
            // 
            // _CmenuAddFile
            // 
            this._CmenuAddFile.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddFile.Image")));
            this._CmenuAddFile.Name = "_CmenuAddFile";
            this._CmenuAddFile.Size = new System.Drawing.Size(145, 22);
            this._CmenuAddFile.Text = "Existing File...";
            this._CmenuAddFile.Click += new System.EventHandler(this._MenuAddFile_Click);
            // 
            // _CmenuAddToInstaller
            // 
            this._CmenuAddToInstaller.Name = "_CmenuAddToInstaller";
            this._CmenuAddToInstaller.Size = new System.Drawing.Size(117, 22);
            this._CmenuAddToInstaller.Text = "Add";
            // 
            // _CmenuAddToPackage
            // 
            this._CmenuAddToPackage.Name = "_CmenuAddToPackage";
            this._CmenuAddToPackage.Size = new System.Drawing.Size(117, 22);
            this._CmenuAddToPackage.Text = "Add";
            // 
            // _CmenuMove
            // 
            this._CmenuMove.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuMoveUp,
            this._CmenuMoveDown});
            this._CmenuMove.Name = "_CmenuMove";
            this._CmenuMove.Size = new System.Drawing.Size(117, 22);
            this._CmenuMove.Text = "Move";
            this._CmenuMove.DropDownOpening += new System.EventHandler(this._CmenuMove_DropDownOpening);
            // 
            // _CmenuMoveUp
            // 
            this._CmenuMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveUp.Image")));
            this._CmenuMoveUp.Name = "_CmenuMoveUp";
            this._CmenuMoveUp.Size = new System.Drawing.Size(138, 22);
            this._CmenuMoveUp.Text = "Move Up";
            this._CmenuMoveUp.Click += new System.EventHandler(this._MenuMoveUp_Click);
            // 
            // _CmenuMoveDown
            // 
            this._CmenuMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuMoveDown.Image")));
            this._CmenuMoveDown.Name = "_CmenuMoveDown";
            this._CmenuMoveDown.Size = new System.Drawing.Size(138, 22);
            this._CmenuMoveDown.Text = "Move Down";
            this._CmenuMoveDown.Click += new System.EventHandler(this._MenuMoveDown_Click);
            // 
            // _CmenuRemove
            // 
            this._CmenuRemove.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuRemove.Image")));
            this._CmenuRemove.Name = "_CmenuRemove";
            this._CmenuRemove.Size = new System.Drawing.Size(117, 22);
            this._CmenuRemove.Text = "Remove";
            this._CmenuRemove.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _CmenuDelete
            // 
            this._CmenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDelete.Image")));
            this._CmenuDelete.Name = "_CmenuDelete";
            this._CmenuDelete.Size = new System.Drawing.Size(117, 22);
            this._CmenuDelete.Text = "Delete";
            this._CmenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _IlistMain
            // 
            this._IlistMain.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistMain.ImageStream")));
            this._IlistMain.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistMain.Images.SetKeyName(0, "project");
            this._IlistMain.Images.SetKeyName(1, "installer");
            this._IlistMain.Images.SetKeyName(2, "package");
            this._IlistMain.Images.SetKeyName(3, "folder");
            this._IlistMain.Images.SetKeyName(4, "settings");
            // 
            // _BasicItemCtrl
            // 
            this._BasicItemCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._BasicItemCtrl.Location = new System.Drawing.Point(3, 4);
            this._BasicItemCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._BasicItemCtrl.Name = "_BasicItemCtrl";
            this._BasicItemCtrl.Size = new System.Drawing.Size(688, 505);
            this._BasicItemCtrl.TabIndex = 0;
            this._BasicItemCtrl.Visible = false;
            this._BasicItemCtrl.ItemChanged += new Robbiblubber.BadWolf.Struct.ItemEventHandler(this._ItemChanged);
            // 
            // _MenuAddToSteps
            // 
            this._MenuAddToSteps.Name = "_MenuAddToSteps";
            this._MenuAddToSteps.Size = new System.Drawing.Size(195, 22);
            this._MenuAddToSteps.Text = "Add";
            // 
            // _MenuAddToPackages
            // 
            this._MenuAddToPackages.Name = "_MenuAddToPackages";
            this._MenuAddToPackages.Size = new System.Drawing.Size(195, 22);
            this._MenuAddToPackages.Text = "Add";
            // 
            // _CmenuAddToSteps
            // 
            this._CmenuAddToSteps.Name = "_CmenuAddToSteps";
            this._CmenuAddToSteps.Size = new System.Drawing.Size(117, 22);
            this._CmenuAddToSteps.Text = "Add";
            // 
            // _CmenuAddToPackages
            // 
            this._CmenuAddToPackages.Name = "_CmenuAddToPackages";
            this._CmenuAddToPackages.Size = new System.Drawing.Size(117, 22);
            this._CmenuAddToPackages.Text = "Add";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 588);
            this.Controls.Add(this._SplitMail);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Text = "Bad Wolf Packager";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this._SplitMail.Panel1.ResumeLayout(false);
            this._SplitMail.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._SplitMail)).EndInit();
            this._SplitMail.ResumeLayout(false);
            this._CmenuTree.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer _SplitMail;
        private System.Windows.Forms.TreeView _TreeMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuNewProject;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpenProject;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveProject;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCloseProject;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddToProject;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddToPackage;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank2;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuView;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddToInstaller;
        private System.Windows.Forms.ToolStripMenuItem _MenuCompile;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ImageList _IlistMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddInstaller;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddPackage;
        private System.Windows.Forms.ToolStripSeparator _MenuAddBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
        private System.Windows.Forms.ToolStripMenuItem _MenuRemove;
        private System.Windows.Forms.ToolStripMenuItem _MenuDelete;
        private Struct.BasicItemControl _BasicItemCtrl;
        private System.Windows.Forms.ToolStripMenuItem _MenuBuild;
        private System.Windows.Forms.ToolStripMenuItem _MenuMove;
        private System.Windows.Forms.ToolStripMenuItem _MenuMoveUp;
        private System.Windows.Forms.ToolStripMenuItem _MenuMoveDown;
        private System.Windows.Forms.ContextMenuStrip _CmenuTree;
        private System.Windows.Forms.ToolStripMenuItem _CmenuClose;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank0;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddToProject;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddInstaller;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddPackage;
        private System.Windows.Forms.ToolStripSeparator _CmenuAddBlank0;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddFile;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddToInstaller;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddToPackage;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMove;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveUp;
        private System.Windows.Forms.ToolStripMenuItem _CmenuMoveDown;
        private System.Windows.Forms.ToolStripMenuItem _CmenuRemove;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDelete;
        private System.Windows.Forms.ToolStripMenuItem _CmenuBuild;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddToSteps;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddToPackages;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddToSteps;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddToPackages;
    }
}

