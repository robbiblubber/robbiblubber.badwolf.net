﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class implements the compiler options window.</summary>
    internal partial class FormCompileOptions: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Checkbox array.</summary>
        private CheckBox[] _Checks;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="levels">Levels.</param>
        public FormCompileOptions(int[] levels)
        {
            InitializeComponent();

            _Checks = new CheckBox[] { _CheckTrace, _CheckDebug, _CheckInformation, _CheckWarning, _CheckError, _CheckCritical };
            Levels = levels;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the selected log levels.</summary>
        public int[] Levels
        {
            get
            {
                List<int> rval = new List<int>();
                for(int i = 0; i < _Checks.Length; i++) { if(_Checks[i].Checked) rval.Add(i); }
                return rval.ToArray();
            }
            set
            {
                for(int i = 0; i < _Checks.Length; i++) { _Checks[i].Checked = value.Contains(i); }
            }
        }
    }
}
