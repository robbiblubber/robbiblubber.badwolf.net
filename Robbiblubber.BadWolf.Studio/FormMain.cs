﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class implements the packager main window.</summary>
    internal partial class FormMain: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        public FormMain(string fileName = null)
        {
            InitializeComponent();

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            if(!string.IsNullOrWhiteSpace(fileName)) _Open(fileName);

            _UpdateRecentFiles();

            _MenuDelete.ShortcutKeyDisplayString = "Del";
            _MenuRemove.ShortcutKeyDisplayString = "Del";

            foreach(IItemController i in ControllerManager.Controllers.Values.OrderBy(m => m.SortIndex))
            {
                if(i.OnInstaller) 
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(i.ItemName, i.ItemImage);
                    m.Tag = i;
                    m.Click += _MenuAddToInstaller_Click;
                    m.ShortcutKeys = i.ItemKeys;

                    _MenuAddToInstaller.DropDownItems.Add(m);

                    m = new ToolStripMenuItem(i.ItemName, i.ItemImage);
                    m.Tag = i;
                    m.Click += _MenuAddToInstaller_Click;

                    _CmenuAddToInstaller.DropDownItems.Add(m);
                }

                if(i.OnPackage)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(i.ItemName, i.ItemImage);
                    m.Tag = i;
                    m.Click += _MenuAddToPackage_Click;
                    m.ShortcutKeys = i.ItemKeys;

                    _MenuAddToPackage.DropDownItems.Add(m);

                    m = new ToolStripMenuItem(i.ItemName, i.ItemImage);
                    m.Tag = i;
                    m.Click += _MenuAddToPackage_Click;

                    _CmenuAddToPackage.DropDownItems.Add(m);
                }

                _IlistMain.Images.Add(i.ItemImageKey, i.ItemImage);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the available packages for an installer.</summary>
        private IEnumerable<Package> _AvailablePackages
        {
            get
            {
                TreeNode n = _TreeMain.SelectedNode;
                if(n == null) return new List<Package>();
                if(!(n.Tag is IInstallerPackageParent)) return new List<Package>();

                
                while(!(n.Tag is InstallerPackagesFolder)) { n = n.Parent; }
                return _Project.Packages.Where(m => (!((InstallerPackagesFolder) n.Tag).AllChildPackages.Contains(m)));
            }
        }


        /// <summary>Adds packages menus to an "Add Packages" menu.</summary>
        /// <param name="menu">Menu.</param>
        /// <returns>Returns TRUE if the menu contains items, otherwise returns FALSE.</returns>
        private bool _AddPackagesMenus(ToolStripMenuItem menu)
        {
            foreach(ToolStripMenuItem i in menu.DropDownItems) { i.Click -= _MenuAddToPackages_Click; }
            menu.DropDownItems.Clear();

            foreach(Package i in _AvailablePackages.OrderBy(m => m.Name))
            {
                ToolStripMenuItem n = new ToolStripMenuItem(i.Name, _IlistMain.Images["package"]);
                n.Tag = i;
                n.Click += _MenuAddToPackages_Click;

                menu.DropDownItems.Add(n);
            }
            return (menu.DropDownItems.Count > 0);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Draws packages.</summary>
        /// <param name="parentNode">Parent tree node.</param>
        /// <param name="packages">Packages parent.</param>
        private void _DrawPackages(TreeNode parentNode, IInstallerPackageParent packages)
        {
            TreeNode n;
            foreach(InstallerPackage i in packages.Packages)
            {
                n = new TreeNode(i.Name);
                n.ImageKey = n.SelectedImageKey = "package";
                n.Tag = i;
                _DrawPackages(n, i);
                
                parentNode.Nodes.Add(n);
            }
        }


        /// <summary>Draws the project.</summary>
        /// <param name="select">Selected item.</param>
        private void _DrawProject(IShowable select = null)
        {
            if(_TreeMain == null) return;

            object sel = _TreeMain.SelectedNode?.Tag;
            List<object> open = _GetOpenTags();

            _TreeMain.BeginUpdate();

            _TreeMain.Nodes.Clear();
            if(_Project == null) { _TreeMain.EndUpdate(); return; }
            
            TreeNode p = new TreeNode(_Project.Name);
            p.ToolTipText = (string.IsNullOrWhiteSpace(_Project.Description) ? null : _Project.Description);
            p.ImageKey = "project";
            p.Tag = _Project;

            _TreeMain.Nodes.Add(p);

            foreach(Installer i in _Project.Installers)
            {
                TreeNode m, n = new TreeNode(i.Name);
                n.ToolTipText = i?.Description;
                n.ImageKey = n.SelectedImageKey = "installer";
                n.Tag = i;

                m = new TreeNode("Settings");
                m.ImageKey = m.SelectedImageKey = "settings";
                m.Tag = i.Settings;
                n.Nodes.Add(m);

                m = new TreeNode("Steps");
                m.ImageKey = m.SelectedImageKey = "folder";
                m.Tag = i.Steps;
                n.Nodes.Add(m);

                m = new TreeNode("Packages");
                m.ImageKey = m.SelectedImageKey = "folder";
                m.Tag = i.Packages;
                _DrawPackages(m, i.Packages);
                n.Nodes.Add(m);
                
                foreach(IItem k in i.Items.OrderBy(w => w.SortIndex))
                {
                    m = new TreeNode(k.Name);
                    m.ToolTipText = k?.Description;
                    m.ImageKey = m.SelectedImageKey = k.GetController().ItemImageKey;
                    m.Tag = k;

                    n.Nodes.Add(m);
                }
                p.Nodes.Add(n);
            }

            foreach(Package i in _Project.Packages)
            {
                TreeNode m, n = new TreeNode(i.Name);
                n.ToolTipText = (string.IsNullOrWhiteSpace(i.Description) ? null : i.Description);
                n.ImageKey = n.SelectedImageKey = "package";
                n.Tag = i;

                foreach(IItem k in i.Items.OrderBy(w => w.SortIndex))
                {
                    m = new TreeNode(k.Name);
                    m.ToolTipText = k?.Description;
                    m.ImageKey = m.SelectedImageKey = k.GetController().ItemImageKey;
                    m.Tag = k;

                    n.Nodes.Add(m);
                }
                p.Nodes.Add(n);
            }

            p.Expand();
            _OpenTags(open);
            _Select(select ?? sel);
            _TreeMain.EndUpdate();
        }


        /// <summary>Selects an item in the tree view.</summary>
        /// <param name="item">Item.</param>
        /// <param name="start">Start tree node.</param>
        /// <returns>Selected node.</returns>
        private TreeNode _Select(object item, TreeNode start = null)
        {
            if(item == null) { return null; }
            if(start == null) { start = _TreeMain.Nodes[0]; }

            if(start.Tag == item)
            {
                _TreeMain.SelectedNode = start;
                start.EnsureVisible();
            }
            else
            {
                TreeNode rval;
                foreach(TreeNode i in start.Nodes)
                {
                    if((rval =_Select(item, i)) != null) { return rval; }
                }
            }

            return null;
        }


        /// <summary>Gets the control for an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Control.</returns>
        private IItemControl _GetControl(IShowable item)
        {
            Type ctype = (item.GetItem().GetController()?.ControlType ?? typeof(BasicItemControl));
            if(item is InstallerSettings) { ctype = typeof(InstallerSettingsControl); }
            if(item is InstallerPackage)  { ctype = typeof(InstallerPackageControl); }

            foreach(Control i in _SplitMail.Panel2.Controls)
            {
                if(i.GetType() == ctype) return (IItemControl) i;
            }

            Control c = (Control) Activator.CreateInstance(ctype);
            _SplitMail.Panel2.Controls.Add(c);
            c.Location = _BasicItemCtrl.Location;
            ((IItemControl) c).ItemChanged += _ItemChanged;

            return (IItemControl) c;
        }


        /// <summary>Shows the control for an item.</summary>
        /// <param name="item">Item.</param>
        private void _ShowControl(IShowable item)
        {
            IItemControl c = _GetControl(item);

            c.ShowItem(item.GetItem(), _Project);
            foreach(Control i in _SplitMail.Panel2.Controls)
            {
                i.Visible = (i == c);
            }
        }


        /// <summary>Opens a project.</summary>
        /// <param name="fileName"></param>
        private void _Open(string fileName)
        {
            try
            {
                _Project = new Project(fileName);
                _DrawProject();

                _RecentFiles.Add(fileName);
                _UpdateRecentFiles();
            }
            catch(Exception ex) 
            {
                ex = new Exception("Could not open file: " + ex.Message, ex);
                DebugOp.DumpMessage(ex, "BWPK00200"); 
            }
        }


        /// <summary>Gets the tags of all open nodes.</summary>
        private List<object> _GetOpenTags()
        {
            List<object> rval = new List<object>();

            if(_TreeMain != null)
            {
                foreach(TreeNode i in _TreeMain.Nodes) { _GetOpenTags(ref rval, i); }
            }
            return rval;
        }


        /// <summary>Gets the tags of all open nodes in a subtree.</summary>
        /// <param name="list">Open node tag list.</param>
        /// <param name="n">Start tree node.</param>
        private void _GetOpenTags(ref List<object> list, TreeNode n)
        {
            if(n.IsExpanded) { list.Add(n.Tag); }
            foreach(TreeNode i in n.Nodes) { _GetOpenTags(ref list, i); }
        }


        /// <summary>Opens tree nodes by their tags.</summary>
        /// <param name="list">List of open node tags.</param>
        /// <param name="n">Start tree node.</param>
        private void _OpenTags(List<object> list, TreeNode n = null)
        {
            if((n != null) && (list.Contains(n.Tag))) { n.Expand(); }

            foreach(TreeNode i in ((n == null) ? _TreeMain.Nodes : n.Nodes))
            {
                _OpenTags(list, i);
            }
        }



        /// <summary>Checks for unsaved changes.</summary>
        /// <returns>Returns TRUE if the operation needs to be cancelled.</returns>
        private bool _CheckSavedCancel()
        {
            if(_Project == null) return false;

            if(_Project.HasChanges)
            {
                switch(MessageBox.Show("The project \"" + _Project.Name + "\" contains unsaved changes. Do you want to save before closing the project?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes: _Project.Save(); break;
                    case DialogResult.Cancel: return true;
                }
            }

            return false;
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), Properties.Resources.project, new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if((_Project != null) && (_Project.FileName != null)) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(_Project.FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("BWPK00109", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Recent file menu item clicked.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            try
            {
                if(_CheckSavedCancel()) return;

                _RecentFiles.Add((string) ((ToolStripMenuItem) sender).Tag);
                _Open((string) ((ToolStripMenuItem) sender).Tag);
            }
            catch(Exception ex) { DebugOp.DumpMessage("BWPK00112", ex); }
        }



        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            FormAbout f = new FormAbout();
            f.ShowDialog();
        }


        /// <summary>Menu "New Project" click.</summary>
        private void _MenuNewProject_Click(object sender, EventArgs e)
        {
            if(_CheckSavedCancel()) return;

            FormNewObject f = new FormNewObject(NewObjectType.PROJECT);

            if(f.ShowDialog() == DialogResult.OK)
            {
                _Project = new Project(f.FileName, f.ID, f.ObjectName, f.Description);
                _DrawProject();

                _RecentFiles.Add(f.FileName);
                _UpdateRecentFiles();

            }
        }


        /// <summary>Menu "Open Project" click.</summary>
        private void _MenuOpenProject_Click(object sender, EventArgs e)
        {
            if(_CheckSavedCancel()) return;

            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Packager Projects (*.bwprj)|*.bwprj|All Files|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                _Open(d.FileName);
            }
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            if((_TreeMain.SelectedNode == null) || (_TreeMain.SelectedNode.Tag is Project))
            {
                _MenuAddToProject.Visible = true;
                _MenuAddToProject.Enabled = (_TreeMain.SelectedNode != null);
                _MenuAddToPackage.Visible = _MenuAddToInstaller.Visible = false;
                _MenuAddToPackages.Visible = _MenuAddToSteps.Visible = false;

                _MenuMove.Enabled = false;
                
                _MenuDelete.Visible = true;
                _MenuDelete.Enabled = false;
                _MenuRemove.Visible = false;
            }
            else if(_TreeMain.SelectedNode.Tag is IContainerItem)
            {
                _MenuAddToProject.Visible = false;
                _MenuAddToInstaller.Visible = (_TreeMain.SelectedNode.Tag is Installer);
                _MenuAddToPackage.Visible = (_TreeMain.SelectedNode.Tag is Package);
                _MenuAddToPackages.Visible = _MenuAddToSteps.Visible = false;

                _MenuMove.Enabled = true;

                _MenuDelete.Visible = false;
                _MenuRemove.Visible = true;
            }
            else if(_TreeMain.SelectedNode.Tag is IInstallerChild)
            {
                _MenuAddToProject.Visible = _MenuAddToInstaller.Visible = _MenuAddToPackage.Visible = false;

                if(_TreeMain.SelectedNode.Tag is IInstallerPackageParent)
                {
                    _MenuAddToPackages.Visible = true;
                    _MenuAddToPackages.Visible = _AddPackagesMenus(_MenuAddToPackages);
                    _MenuAddToSteps.Visible = false;
                    _MenuMove.Enabled = true;

                    _MenuDelete.Visible = false;
                    _MenuRemove.Visible = true;
                }
                else if(_TreeMain.SelectedNode.Tag is InstallerStepsFolder)
                {
                    _MenuAddToSteps.Visible = false;
                    _MenuAddToSteps.Enabled = false;
                    _MenuAddToPackages.Visible = true;
                    _MenuMove.Enabled = false;

                    _MenuDelete.Visible = true;
                    _MenuDelete.Enabled = false;
                    _MenuRemove.Visible = false;
                }
                else
                {
                    _MenuAddToSteps.Visible = true;
                    _MenuAddToSteps.Enabled = false;
                    _MenuAddToPackages.Visible = false;
                    _MenuMove.Enabled = false;

                    _MenuDelete.Visible = true;
                    _MenuDelete.Enabled = false;
                    _MenuRemove.Visible = false;
                }
            }
            else
            {
                _MenuAddToProject.Visible = true;
                _MenuAddToProject.Enabled = false;
                _MenuAddToInstaller.Visible = _MenuAddToPackage.Visible = _MenuAddToSteps.Visible = _MenuAddToPackages.Visible = false;

                _MenuMove.Enabled = true;

                _MenuDelete.Visible = true;
                _MenuDelete.Enabled = true;
                _MenuRemove.Visible = false;
            }
        }


        /// <summary>Context menu opening.</summary>
        private void _CmenuTree_Opening(object sender, CancelEventArgs e)
        {
            if((_TreeMain.SelectedNode == null) || (_TreeMain.SelectedNode.Tag is Project))
            {
                _CmenuClose.Visible = (_TreeMain.SelectedNode != null);
                _CmenuBuild.Visible = (_TreeMain.SelectedNode != null);
                _CmenuTreeBlank0.Visible = (_TreeMain.SelectedNode != null);

                _CmenuAddToProject.Visible = true;
                _CmenuAddToProject.Enabled = (_TreeMain.SelectedNode != null);
                _CmenuAddToPackage.Visible = _CmenuAddToInstaller.Visible = false;
                _CmenuAddToPackages.Visible = _CmenuAddToSteps.Visible = false;

                _CmenuMove.Enabled = false;

                _CmenuDelete.Visible = true;
                _CmenuDelete.Enabled = false;
                _CmenuRemove.Visible = false;
            }
            else if(_TreeMain.SelectedNode.Tag is IContainerItem)
            {
                _CmenuClose.Visible = false;
                _CmenuBuild.Visible = _CmenuTreeBlank0.Visible = true;

                _CmenuAddToProject.Visible = false;
                _CmenuAddToInstaller.Visible = (_TreeMain.SelectedNode.Tag is Installer);
                _CmenuAddToPackage.Visible = (_TreeMain.SelectedNode.Tag is Package);
                _CmenuAddToPackages.Visible = _CmenuAddToSteps.Visible = false;

                _CmenuMove.Enabled = true;

                _CmenuDelete.Visible = false;
                _CmenuRemove.Visible = true;
            }
            else if(_TreeMain.SelectedNode.Tag is IInstallerChild)
            {
                _CmenuClose.Visible = false;
                _CmenuBuild.Visible = _CmenuTreeBlank0.Visible = false;

                _CmenuAddToProject.Visible = _CmenuAddToInstaller.Visible = _CmenuAddToPackage.Visible = false;

                if(_TreeMain.SelectedNode.Tag is IInstallerPackageParent)
                {
                    _CmenuAddToPackages.Visible = true;
                    _CmenuAddToPackages.Enabled = _AddPackagesMenus(_CmenuAddToPackages);
                    _CmenuAddToSteps.Visible = false;
                    _CmenuMove.Enabled = true;

                    _CmenuDelete.Visible = false;
                    _CmenuRemove.Visible = true;
                }
                else if(_TreeMain.SelectedNode.Tag is InstallerStepsFolder)
                {
                    _CmenuAddToSteps.Visible = false;
                    _CmenuAddToSteps.Enabled = false;
                    _CmenuAddToPackages.Visible = true;
                    _CmenuMove.Enabled = false;

                    _CmenuDelete.Visible = true;
                    _CmenuDelete.Enabled = false;
                    _CmenuRemove.Visible = false;
                }
                else
                {
                    _CmenuAddToSteps.Visible = true;
                    _CmenuAddToSteps.Enabled = false;
                    _CmenuAddToPackages.Visible = false;
                    _CmenuMove.Enabled = false;

                    _CmenuDelete.Visible = true;
                    _CmenuDelete.Enabled = false;
                    _CmenuRemove.Visible = false;
                }
            }
            else
            {
                _CmenuClose.Visible = false;
                _CmenuBuild.Visible = _CmenuTreeBlank0.Visible = true;

                _CmenuAddToProject.Visible = true;
                _CmenuAddToProject.Enabled = false;
                _CmenuAddToInstaller.Visible = _CmenuAddToPackage.Visible = _CmenuAddToSteps.Visible = _CmenuAddToPackages.Visible = false;

                _CmenuMove.Enabled = true;

                _CmenuDelete.Visible = true;
                _CmenuDelete.Enabled = true;
                _CmenuRemove.Visible = false;
            }
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSaveProject_Click(object sender, EventArgs e)
        {
            _Project.Save();
        }


        /// <summary>Menu "Add Package" click.</summary>
        private void _MenuAddPackage_Click(object sender, EventArgs e)
        {
            FormNewObject f = new FormNewObject(NewObjectType.PACKAGE, _Project);

            if(f.ShowDialog() == DialogResult.OK)
            {
                _Project.Packages.Add(new Package(_Project, f.FileName, f.ID, f.ObjectName, f.Description));
                _DrawProject();
            }
        }


        /// <summary>Menu "Add Installer" click.</summary>
        private void _MenuAddInstaller_Click(object sender, EventArgs e)
        {
            FormNewObject f = new FormNewObject(NewObjectType.INSTALLER, _Project);

            if(f.ShowDialog() == DialogResult.OK)
            {
                _Project.Installers.Add(new Installer(_Project, f.FileName, f.ID, f.ObjectName, f.Description));
                _DrawProject();
            }
        }


        /// <summary>Menu "Add To Installer" click.</summary>
        private void _MenuAddToInstaller_Click(object sender, EventArgs e)
        {
            IItem item = (IItem) Activator.CreateInstance(((IItemController) ((ToolStripItem) sender).Tag).ItemType, (DdpSection) null);
            ((Installer) _TreeMain.SelectedNode.Tag).Items.Add(item);
            _DrawProject(item);
        }


        /// <summary>Menu "Add To Package" click.</summary>
        private void _MenuAddToPackage_Click(object sender, EventArgs e)
        {
            IItem item = (IItem) Activator.CreateInstance(((IItemController) ((ToolStripItem) sender).Tag).ItemType, (DdpSection) null);
            ((Package) _TreeMain.SelectedNode.Tag).Items.Add(item);
            _DrawProject(item);
        }


        /// <summary>After tree select.</summary>
        private void _TreeMain_AfterSelect(object sender, TreeViewEventArgs e)
        {
            _ShowControl((IShowable) _TreeMain.SelectedNode.Tag);
        }


        /// <summary>Menu "Build" click.</summary>
        private void _MenuBuild_Click(object sender, EventArgs e)
        {
            IItem item = null;
            if(_TreeMain.SelectedNode != null) { item = (IItem) _TreeMain.SelectedNode.Tag; }

            FormCompile f = new FormCompile(_Project, item);
            f.ShowDialog();
        }


        /// <summary>Item changed.</summary>
        private void _ItemChanged(object sender, ItemEventArgs e)
        {
            if((_TreeMain.SelectedNode != null) && (_TreeMain.SelectedNode.Tag is IItem))
            {
                _TreeMain.SelectedNode.Text = ((IItem) _TreeMain.SelectedNode.Tag).Name; 
            }
        }


        /// <summary>Menu "Move" opening.</summary>
        private void _MenuMove_DropDownOpening(object sender, EventArgs e)
        {
            _MenuMoveUp.Enabled = _Project.CanMoveUp((IShowable) _TreeMain.SelectedNode.Tag);
            _MenuMoveDown.Enabled = _Project.CanMoveDown((IShowable) _TreeMain.SelectedNode.Tag);
        }


        /// <summary>Context menu "Move" opening.</summary>
        private void _CmenuMove_DropDownOpening(object sender, EventArgs e)
        {
            _CmenuMoveUp.Enabled = _Project.CanMoveUp((IShowable) _TreeMain.SelectedNode.Tag);
            _CmenuMoveDown.Enabled = _Project.CanMoveDown((IShowable) _TreeMain.SelectedNode.Tag);
        }


        /// <summary>Menu "Move up" click.</summary>
        private void _MenuMoveUp_Click(object sender, EventArgs e)
        {
            _Project.MoveUp((IItem) _TreeMain.SelectedNode.Tag);
            _DrawProject();
        }


        /// <summary>Menu "Move down" click.</summary>
        private void _MenuMoveDown_Click(object sender, EventArgs e)
        {
            _Project.MoveDown((IItem) _TreeMain.SelectedNode.Tag);
            _DrawProject();
        }


        /// <summary>Menu "File" closed.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            _MenuMove.Enabled = _MenuMoveUp.Enabled = _MenuMoveDown.Enabled;
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Menu "Delete"/"Remove" click.</summary>
        private void _MenuDelete_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(_TreeMain.SelectedNode.Tag is Project) return;

            string cptn = "Delete";
            string text = "Do you want to delete the item \"" + _TreeMain.SelectedNode.Text + "\"?";
            if(_TreeMain.SelectedNode.Tag is Installer)
            {
                cptn = "Remove";
                text = "Do you want to remove the installer \"" + _TreeMain.SelectedNode.Text + "\" from this project?";
            }
            else if(_TreeMain.SelectedNode.Tag is Package)
            {
                cptn = "Remove";
                text = "Do you want to remove the package \"" + _TreeMain.SelectedNode.Text + "\" from this project?";
            }
            else if(_TreeMain.SelectedNode.Tag is InstallerPackage)
            {
                cptn = "Remove";
                text = "Do you want to remove the package \"" + _TreeMain.SelectedNode.Text + "\" from this installer?";
            }

            if(MessageBox.Show(text, cptn, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes) return;

            IItem sel = _Project;
            if(_TreeMain.SelectedNode.Tag is Installer)
            {
                _Project.Installers.Remove((Installer) _TreeMain.SelectedNode.Tag);
            }
            else if(_TreeMain.SelectedNode.Tag is Package)
            {
                _Project.Packages.Remove((Package) _TreeMain.SelectedNode.Tag);
            }
            else if(_TreeMain.SelectedNode.Tag is InstallerPackage)
            {
                ((InstallerPackage) _TreeMain.SelectedNode.Tag).Remove();
            }
            else
            {
                sel = _Project.GetParent((IItem) _TreeMain.SelectedNode.Tag);
                ((IContainerItem) sel).Items.Remove((IItem) _TreeMain.SelectedNode.Tag);
            }

            _DrawProject(sel);
        }


        /// <summary>Menu "Close" click.</summary>
        private void _MenuCloseProject_Click(object sender, EventArgs e)
        {
            if(_CheckSavedCancel()) return;

            _Project = null;
            _UpdateRecentFiles();
            _DrawProject();
        }


        /// <summary>Tree mouse down.</summary>
        private void _TreeMain_MouseDown(object sender, MouseEventArgs e)
        {
            _TreeMain.SelectedNode = _TreeMain.HitTest(e.Location).Node;
        }


        /// <summary>Menu "Add Existing File" click.</summary>
        private void _MenuAddFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Bad Wolf Files (*.bwnst, *.bwpkg)|*.bwnst;*.bwpkg|Bad Wolf Installers (*.bwnst)|*.bwnst|Bad Wolf Packages (*.pwpkg)|*.bwpkg|All Files|*.*";

            if(d.ShowDialog() != DialogResult.OK) return;

            IItem item = null;
            if(d.FileName.ToLower().EndsWith(".bwnst"))
            {
                Installer f = null;
                try
                {
                    f = new Installer(_Project, d.FileName);
                }
                catch(Exception ex) { MessageBox.Show(ex.Message, "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }

                switch(_Project.CheckID(f.ID))
                {
                    case -1:
                    case -2: MessageBox.Show("The installer file is invalid.", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Error); return;
                    case -3: MessageBox.Show("The project already contains an item with the ID \"" + f.ID + "\".", "Duplicate Item", MessageBoxButtons.OK, MessageBoxIcon.Error); return;
                }

                item = f;
                _Project.Installers.Add(f);
            }
            else
            {
                Package f = null;
                try
                {
                    f = new Package(_Project, d.FileName);
                }
                catch(Exception ex) { MessageBox.Show(ex.Message, "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }

                switch(_Project.CheckID(f.ID))
                {
                    case -1:
                    case -2: MessageBox.Show("The package file is invalid.", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Error); return;
                    case -3: MessageBox.Show("The project already contains an item with the ID \"" + f.ID + "\".", "Duplicate Item", MessageBoxButtons.OK, MessageBoxIcon.Error); return;
                }

                if(_Project.GetPackage(f.ID) != null) { f = _Project.GetPackage(f.ID); }

                item = f;
                _Project.Packages.Add(f);
            }

            _DrawProject(item);
        }


        /// <summary>Menu "Add Package" (to installer) click.</summary>
        private void _MenuAddToPackages_Click(object sender, EventArgs e)
        {
            if((_TreeMain.SelectedNode == null) || (!(_TreeMain.SelectedNode.Tag is InstallerPackageParent))) return;

            InstallerPackage p = new InstallerPackage(((InstallerPackageParent) _TreeMain.SelectedNode.Tag).Installer, (Package) ((ToolStripMenuItem) sender).Tag);
            ((InstallerPackageParent) _TreeMain.SelectedNode.Tag).Packages.Add(p);

            _DrawProject(p);
        }
    }
}
