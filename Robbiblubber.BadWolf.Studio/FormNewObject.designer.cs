﻿namespace Robbiblubber.BadWolf.Packager
{
    partial class FormNewObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewObject));
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._ComboFile = new System.Windows.Forms.ComboBox();
            this._LabelFile = new System.Windows.Forms.Label();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelNameError = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelFileError = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _TextName
            // 
            this._TextName.Location = new System.Drawing.Point(41, 45);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(38, 29);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _ComboFile
            // 
            this._ComboFile.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this._ComboFile.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this._ComboFile.FormattingEnabled = true;
            this._ComboFile.Location = new System.Drawing.Point(41, 154);
            this._ComboFile.Name = "_ComboFile";
            this._ComboFile.Size = new System.Drawing.Size(622, 25);
            this._ComboFile.TabIndex = 2;
            this._ComboFile.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelFile
            // 
            this._LabelFile.AutoSize = true;
            this._LabelFile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFile.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFile.Location = new System.Drawing.Point(38, 138);
            this._LabelFile.Name = "_LabelFile";
            this._LabelFile.Size = new System.Drawing.Size(28, 13);
            this._LabelFile.TabIndex = 2;
            this._LabelFile.Text = "&File:";
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(669, 154);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 2;
            this._ButtonBrowse.TabStop = false;
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(522, 372);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(141, 29);
            this._ButtonCancel.TabIndex = 5;
            this._ButtonCancel.Tag = "";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // _ButtonOK
            // 
            this._ButtonOK.Enabled = false;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(375, 372);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(141, 29);
            this._ButtonOK.TabIndex = 4;
            this._ButtonOK.Tag = "";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.SystemColors.Info;
            this._TextDescription.Location = new System.Drawing.Point(41, 215);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 3;
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(38, 199);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 3;
            this._LabelDescription.Text = "&Description:";
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(38, 83);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.Location = new System.Drawing.Point(41, 99);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(17, 45);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._ToolTip.SetToolTip(this._LabelNameError, "Name is empty");
            this._LabelNameError.Visible = false;
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(17, 99);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 1;
            this._ToolTip.SetToolTip(this._LabelIdError, "ID is empty");
            // 
            // _LabelFileError
            // 
            this._LabelFileError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFileError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFileError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelFileError.Image")));
            this._LabelFileError.Location = new System.Drawing.Point(17, 154);
            this._LabelFileError.Name = "_LabelFileError";
            this._LabelFileError.Size = new System.Drawing.Size(25, 25);
            this._LabelFileError.TabIndex = 2;
            this._ToolTip.SetToolTip(this._LabelFileError, "File is empty");
            this._LabelFileError.Visible = false;
            // 
            // FormNewObject
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(729, 427);
            this.Controls.Add(this._ComboFile);
            this.Controls.Add(this._LabelFileError);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._LabelFile);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNewObject";
            this.Text = "New Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.ComboBox _ComboFile;
        private System.Windows.Forms.Label _LabelFile;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.Label _LabelFileError;
    }
}