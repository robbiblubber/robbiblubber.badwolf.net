﻿using System;



namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class specifies new object types.</summary>
    public enum NewObjectType: int
    {
        /// <summary>The new object is a project.</summary>
        PROJECT   = 0,
        /// <summary>The new object is a package.</summary>
        PACKAGE   = 1,
        /// <summary>The new object is an installer.</summary>
        INSTALLER = 2
    }
}
