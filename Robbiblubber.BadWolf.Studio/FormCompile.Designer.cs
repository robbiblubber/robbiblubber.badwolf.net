﻿namespace Robbiblubber.BadWolf.Packager
{
    partial class FormCompile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCompile));
            this._LabelName = new System.Windows.Forms.Label();
            this._CboSource = new Robbiblubber.Util.Controls.RichComboBox();
            this._IlistCompile = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this._CboTarget = new System.Windows.Forms.ComboBox();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._ListOut = new System.Windows.Forms.ListView();
            this._ChText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._IlistOut = new System.Windows.Forms.ImageList(this.components);
            this._ButtonOptions = new System.Windows.Forms.Button();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ButtonClose = new System.Windows.Forms.Button();
            this._ButtonCompile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(23, 21);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(45, 13);
            this._LabelName.TabIndex = 1;
            this._LabelName.Text = "&Source:";
            // 
            // _CboSource
            // 
            this._CboSource.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._CboSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboSource.FormattingEnabled = true;
            this._CboSource.ImageList = this._IlistCompile;
            this._CboSource.Location = new System.Drawing.Point(26, 37);
            this._CboSource.Name = "_CboSource";
            this._CboSource.Size = new System.Drawing.Size(731, 26);
            this._CboSource.TabIndex = 2;
            // 
            // _IlistCompile
            // 
            this._IlistCompile.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistCompile.ImageStream")));
            this._IlistCompile.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistCompile.Images.SetKeyName(0, "project");
            this._IlistCompile.Images.SetKeyName(1, "installer");
            this._IlistCompile.Images.SetKeyName(2, "package");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(23, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "&Target Folder:";
            // 
            // _CboTarget
            // 
            this._CboTarget.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this._CboTarget.FormattingEnabled = true;
            this._CboTarget.Location = new System.Drawing.Point(26, 88);
            this._CboTarget.Name = "_CboTarget";
            this._CboTarget.Size = new System.Drawing.Size(731, 25);
            this._CboTarget.TabIndex = 4;
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(763, 87);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 5;
            this._ButtonBrowse.TabStop = false;
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(54, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "&Output:";
            // 
            // _ListOut
            // 
            this._ListOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ListOut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._ChText});
            this._ListOut.FullRowSelect = true;
            this._ListOut.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._ListOut.HideSelection = false;
            this._ListOut.Location = new System.Drawing.Point(26, 214);
            this._ListOut.Name = "_ListOut";
            this._ListOut.Size = new System.Drawing.Size(762, 283);
            this._ListOut.SmallImageList = this._IlistOut;
            this._ListOut.TabIndex = 7;
            this._ListOut.UseCompatibleStateImageBehavior = false;
            this._ListOut.View = System.Windows.Forms.View.Details;
            // 
            // _ChText
            // 
            this._ChText.Text = "Text";
            this._ChText.Width = 730;
            // 
            // _IlistOut
            // 
            this._IlistOut.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistOut.ImageStream")));
            this._IlistOut.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistOut.Images.SetKeyName(0, "0");
            this._IlistOut.Images.SetKeyName(1, "1");
            this._IlistOut.Images.SetKeyName(2, "2");
            this._IlistOut.Images.SetKeyName(3, "3");
            this._IlistOut.Images.SetKeyName(4, "4");
            this._IlistOut.Images.SetKeyName(5, "5");
            // 
            // _ButtonOptions
            // 
            this._ButtonOptions.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonOptions.Image")));
            this._ButtonOptions.Location = new System.Drawing.Point(26, 186);
            this._ButtonOptions.Name = "_ButtonOptions";
            this._ButtonOptions.Size = new System.Drawing.Size(25, 25);
            this._ButtonOptions.TabIndex = 8;
            this._ToolTip.SetToolTip(this._ButtonOptions, "Options");
            this._ButtonOptions.UseVisualStyleBackColor = true;
            this._ButtonOptions.Click += new System.EventHandler(this._ButtonOptions_Click);
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(616, 153);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(141, 29);
            this._ButtonClose.TabIndex = 10;
            this._ButtonClose.Tag = "";
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            // 
            // _ButtonCompile
            // 
            this._ButtonCompile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCompile.Location = new System.Drawing.Point(469, 153);
            this._ButtonCompile.Name = "_ButtonCompile";
            this._ButtonCompile.Size = new System.Drawing.Size(141, 29);
            this._ButtonCompile.TabIndex = 9;
            this._ButtonCompile.Tag = "";
            this._ButtonCompile.Text = "&Compile";
            this._ButtonCompile.UseVisualStyleBackColor = true;
            this._ButtonCompile.Click += new System.EventHandler(this._ButtonCompile_Click);
            // 
            // FormCompile
            // 
            this.AcceptButton = this._ButtonCompile;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(819, 509);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._ButtonCompile);
            this.Controls.Add(this._ButtonOptions);
            this.Controls.Add(this._ListOut);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._CboTarget);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._CboSource);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCompile";
            this.Text = "Compile";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCompile_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelName;
        private Util.Controls.RichComboBox _CboSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _CboTarget;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView _ListOut;
        private System.Windows.Forms.Button _ButtonOptions;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.ImageList _IlistCompile;
        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.Button _ButtonCompile;
        private System.Windows.Forms.ColumnHeader _ChText;
        private System.Windows.Forms.ImageList _IlistOut;
    }
}