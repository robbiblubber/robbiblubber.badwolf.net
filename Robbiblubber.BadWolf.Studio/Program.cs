﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Packager
{
    /// <summary>This class implements the main entry point.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Main form.</summary>
        internal static FormMain _Main;

        /// <summary>Splash form.</summary>
        internal static FormSplash _Splash;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Command line arguments.</param>
        [STAThread]
        static void Main(string[] args)
        {
            PathOp.ApplicationPart = "robbiblubber.org/BadWolf/Packager";
            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            _Splash = new FormSplash();
            _Splash.Show();
            Application.DoEvents();

            Thread.Sleep(1600);

            string file = null;
            if(args.Length > 0) { file = args.Chain(" "); }

            _Main = new FormMain(file);
            _Splash.DelayClose();
            Application.Run(_Main);
        }
    }
}
