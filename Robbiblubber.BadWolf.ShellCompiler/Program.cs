﻿using System;
using System.Collections.Generic;

using Robbiblubber.BadWolf.Struct;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.ShellCompiler
{
    /// <summary>This class implements the Bad Wolf Shell Compiler.</summary>
    class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Selected log levels.</summary>
        private static List<LogLevel> _Levels = new List<LogLevel>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Command line arguments.</param>
        /// <returns>Returns the success state.</returns>
        static int Main(string[] args)
        {
            string file = null;
            string outp = null;
            string id = null;

            ConsoleColor col = Console.ForegroundColor;

            try
            {
                for(int i = 0; i < args.Length; i++)
                {
                    if(args[i].Trim().StartsWith("-") || args[i].Trim().StartsWith("/"))
                    {
                        string flg = args[i].Trim(' ', '\t', '-', '/');

                        if(flg == "f") { file = args[++i]; continue; }
                        if(flg == "o") { outp = args[++i]; continue; }
                        if(flg == "x") { id   = args[++i]; continue; }
                        if(flg == "?") { _ShowHelp(); continue; }

                        if(flg.Contains("a")) { _Levels.Add(LogLevel.TRACE); _Levels.Add(LogLevel.DEBUG); _Levels.Add(LogLevel.INFORMATION); _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("A")) { _Levels.Add(LogLevel.TRACE); _Levels.Add(LogLevel.DEBUG); _Levels.Add(LogLevel.INFORMATION); _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("T")) { _Levels.Add(LogLevel.TRACE); _Levels.Add(LogLevel.DEBUG); _Levels.Add(LogLevel.INFORMATION); _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("D")) { _Levels.Add(LogLevel.DEBUG); _Levels.Add(LogLevel.INFORMATION); _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("I")) { _Levels.Add(LogLevel.INFORMATION); _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("W")) { _Levels.Add(LogLevel.WARNING); _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("E")) { _Levels.Add(LogLevel.ERROR); _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("C")) { _Levels.Add(LogLevel.CRITICAL); }
                        if(flg.Contains("t")) { _Levels.Add(LogLevel.TRACE); }
                        if(flg.Contains("d")) { _Levels.Add(LogLevel.DEBUG); }
                        if(flg.Contains("i")) { _Levels.Add(LogLevel.INFORMATION); }
                        if(flg.Contains("w")) { _Levels.Add(LogLevel.WARNING); }
                        if(flg.Contains("e")) { _Levels.Add(LogLevel.ERROR); }
                        if(flg.Contains("c")) { _Levels.Add(LogLevel.CRITICAL); }
                        continue;
                    }

                    if(file == null) { file = args[i]; continue; }
                    if(outp == null) { outp = args[i]; continue; }
                    if(id == null)   { id   = args[i]; continue; }
                }
            }
            catch(Exception) { Console.WriteLine("bwc: invalid input."); return -1; }

            if(file == null) { Console.WriteLine("bwc: no input specified."); return -1; }

            if(outp == null) { outp = PathOp.ApplicationDirectory + @"\out"; }
            IItem item;

            try
            {
                if(file.ToLower().EndsWith(".bwpkg"))
                {
                    item = new Package(new Project(), file);
                }
                else if(file.ToLower().EndsWith(".bwnst"))
                {
                    item = new Installer(new Project(), file);
                }
                else
                {
                    item = new Project(file);
                }
            }
            catch(Exception) { Console.WriteLine("bwc: input file not found."); return -1; }

            if(id != null)
            {
                item = ((IContainerItem) item).GetItem(id);
            }
            if(item == null) { Console.WriteLine("bwc: input id not found."); return -1; }
            
            Compiler c = new Compiler(item, outp);
            c.Feedback += _Feedback;

            item.Compile(c);
            
            Console.ForegroundColor = col;
            return 0;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static void _ShowHelp()
        {
            Console.WriteLine("bwc -?|[-a][-A][-T][-D][-I][-W][-E][-C][-t][-d][-i][-w][-[e][-c] [-f] source [[-o] output] [[-x] id]");
            Console.WriteLine();
            Console.WriteLine("   -?        Shows this help");
            Console.WriteLine("   -a        Enables all log levels");
            Console.WriteLine("   -A        Enables all log levels");
            Console.WriteLine("   -T        Enables all log level TRACE and above");
            Console.WriteLine("   -D        Enables all log level DEBUG and above");
            Console.WriteLine("   -I        Enables all log level INFORMATION and above");
            Console.WriteLine("   -W        Enables all log level WARNING and above");
            Console.WriteLine("   -E        Enables all log level ERROR and above");
            Console.WriteLine("   -C        Enables all log level CRITICAL and above");
            Console.WriteLine("   -t        Enables all log level TRACE");
            Console.WriteLine("   -d        Enables all log level DEBUG");
            Console.WriteLine("   -i        Enables all log level INFORMATION");
            Console.WriteLine("   -w        Enables all log level WARNING");
            Console.WriteLine("   -e        Enables all log level ERROR");
            Console.WriteLine("   -c        Enables all log level CRITICAL");
            Console.WriteLine("   -f        Makes the next argument the source file");
            Console.WriteLine("   -o        Makes the next argument the output directory");
            Console.WriteLine("   -x        Makes the next argument the item ID");
            Console.WriteLine("   source    Source file path");
            Console.WriteLine("   output    Output directory path");
            Console.WriteLine("   id        Target item ID");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compiler feedback.</summary>
        private static void _Feedback(object sender, FeedbackEventArgs e)
        {
            if(!_Levels.Contains(e.Level)) return;

            switch(e.Level)
            {
                case LogLevel.TRACE:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("TRACE   ");
                    break;
                case LogLevel.DEBUG:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("DEBUG   ");
                    break;
                case LogLevel.INFORMATION:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("INFO    ");
                    break;
                case LogLevel.WARNING:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("WARNG   ");
                    break;
                case LogLevel.ERROR:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("ERROR   ");
                    break;
                case LogLevel.CRITICAL:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write("CRTCL   ");
                    break;
            }

            Console.WriteLine(e.Message);
        }
    }
}
