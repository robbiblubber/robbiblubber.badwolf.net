﻿using System;

using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Scripting
{
    /// <summary>This interface provides setup information.</summary>
    public interface ISetupInfo
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the current setup mode.</summary>
        SetupMode Mode { get; }


        /// <summary>Gets the installation target root directory.</summary>
        string TargetDirectory { get; }


        /// <summary>Gets the setup (packages) root directory.</summary>
        string SetupDirectory { get; }


        /// <summary>Gets all defined variables.</summary>
        IImmutableDictionary<string, string> Variables { get; }


        /// <summary>Gets the current setup step.</summary>
        ISetupStep CurrentStep { get; }
    }
}
