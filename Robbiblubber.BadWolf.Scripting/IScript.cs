﻿using System;



namespace Robbiblubber.BadWolf.Scripting
{
    /// <summary>Scripts implement this interface</summary>
    public interface IScript
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This method is called before a setup item is executed.</summary>
        /// <param name="info">Setup information.</param>
        /// <returns>Returns TRUE if the script succeeded, otherwise returns FALSE.</returns>
        bool Before(ISetupInfo info);


        /// <summary>This method is called before a setup item is executed.</summary>
        /// <param name="info">Setup information.</param>
        /// <returns>Returns TRUE if the script succeeded, otherwise returns FALSE.</returns>
        bool After(ISetupStep info);
    }
}
