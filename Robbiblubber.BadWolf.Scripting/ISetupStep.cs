﻿using System;



namespace Robbiblubber.BadWolf.Scripting
{
    /// <summary>This interface provides information about a setup step.</summary>
    public interface ISetupStep
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the setup step ID.</summary>
        string ID { get; }


        /// <summary>Gets the setup type string.</summary>
        string Type { get; }
    }
}
