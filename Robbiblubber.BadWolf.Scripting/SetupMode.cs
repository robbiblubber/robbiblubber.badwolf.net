﻿using System;



namespace Robbiblubber.BadWolf.Scripting
{
    /// <summary>This enumeration defines setup modes.</summary>
    public enum SetupMode
    {
        /// <summary>Setup process is a primary installation.</summary>
        INSTALL   = 0,
        /// <summary>Setup process is an update installation.</summary>
        UPDATE    = 1,
        /// <summary>Setup process is an uninstallation.</summary>
        UNINSTALL = 2
    }
}
