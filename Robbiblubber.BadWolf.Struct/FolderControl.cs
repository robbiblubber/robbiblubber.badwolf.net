﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements a folder control.</summary>
    public partial class FolderControl: UserControl, IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        private Folder _Item;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FolderControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemControl                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        public event ItemEventHandler ItemChanged;


        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        public Control GetControl()
        {
            return this;
        }


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        public void ShowItem(IItem item, Project project)
        {
            _Item = (Folder) item;
            _Project = project;

            _Active = false;
            _TextName.Text = _Item.Name;
            _TextID.Text = _Item.ID;
            _TextCondition.Text = _Item.Condition;
            _TextDescription.Text = _Item.Description;
            _TextCopy.Text = _Item.CopyJob;

            if(_LabelTarget.Visible = _TextTarget.Visible =_Item.HasTarget)
            {
                _TextTarget.Text = ((PayloadFolder) _Item).Target;

                _PanelSouth.Top = 171;
            }
            else
            {
                _PanelSouth.Top = 121;
            }
            _Active = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Item changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            bool idchanged = (_Item.ID != _TextID.Text);
            if(_Active)
            {
                _Item.Name = _TextName.Text;
                _Item.ID = _TextID.Text;
                _Item.Description = _TextDescription.Text;
                _Item.CopyJob = _TextCopy.Text;

                if(_Item.HasTarget) { ((PayloadFolder) _Item).Target = _TextTarget.Text; }

                ItemChanged?.Invoke(this, new ItemEventArgs(_Item));
            }

            _LabelConditionHint.Visible = (!string.IsNullOrEmpty(_TextCondition.Text));
            _LabelNameError.Visible = string.IsNullOrEmpty(_TextName.Text);
            _LabelTargetWarning.Visible = (_Item.HasTarget ? string.IsNullOrWhiteSpace(((PayloadFolder) _Item).Target) : false);
            _LabelCopyWarning.Visible = string.IsNullOrEmpty(_Item.CopyJob);

            switch(_Project.CheckID(_Item))
            {
                case -1:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is empty");
                    _LabelIdError.Visible = true;
                    break;
                case -2:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is invalid");
                    _LabelIdError.Visible = true;
                    break;
                case -3:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is ambiguous");
                    _LabelIdError.Visible = true;
                    break;
                default:
                    _LabelIdError.Visible = false; break;
            }
        }


        /// <summary>Item validating.</summary>
        private void _Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = (_LabelIdError.Visible || _LabelNameError.Visible);
        }
    }
}
