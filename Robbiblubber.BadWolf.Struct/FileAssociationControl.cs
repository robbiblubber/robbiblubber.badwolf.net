﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a file association control.</summary>
    public partial class FileAssociationControl: UserControl, IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        private FileAssociation _Item;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FileAssociationControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemControl                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        public event ItemEventHandler ItemChanged;


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        public void ShowItem(IItem item, Project project)
        {
            _Active = false;
            _Item = (FileAssociation) item;
            _Project = project;

            _TextName.Text = item.Name;
            _TextID.Text = item.ID;
            _TextCondition.Text = item.Condition;
            _TextDescription.Text = item.Description;

            _TextNameLKey.Text = ((ILocalizable) item).NameLocaleKey;
            _TextDecriptionLKey.Text = ((ILocalizable) item).DescriptionLocaleKey;

            _TextExtension.Text = _Item.Extension;
            _TextFilePath.Text = _Item.FilePath;
            _TextIconPath.Text = _Item.IconPath;
            _TextIconIndex.Text = _Item.IconIndex.ToString();

            _Active = true;
            Visible = true;
        }


        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        public Control GetControl()
        {
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            bool idchanged = (_Item.ID != _TextID.Text);
            if(_Active)
            {
                _Item.Name = _TextName.Text;
                _Item.ID = _TextID.Text;
                _Item.Description = _TextDescription.Text;

                _Item.NameLocaleKey = _TextNameLKey.Text;
                _Item.DescriptionLocaleKey = _TextDecriptionLKey.Text;

                _Item.Extension = _TextExtension.Text;
                _Item.FilePath = _TextFilePath.Text;
                _Item.IconPath = _TextIconPath.Text;
                if(int.TryParse(_TextIconIndex.Text, out int n)) { _Item.IconIndex = n; }

                ItemChanged?.Invoke(this, new ItemEventArgs(_Item));
            }

            _LabelNameError.Visible = string.IsNullOrEmpty(_TextName.Text);

            switch(_Project.CheckID(_Item))
            {
                case -1:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is empty");
                    _LabelIdError.Visible = true;
                    break;
                case -2:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is invalid");
                    _LabelIdError.Visible = true;
                    break;
                case -3:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is ambiguous");
                    _LabelIdError.Visible = true;
                    break;
                default:
                    _LabelIdError.Visible = false; break;
            }

            _LabelLocaleHint.Visible = (!(string.IsNullOrEmpty(_TextNameLKey.Text) && string.IsNullOrEmpty(_TextDecriptionLKey.Text)));
            _LabelConditionHint.Visible = (!string.IsNullOrEmpty(_TextCondition.Text));

            _LabelExtensionWarning.Visible = string.IsNullOrWhiteSpace(_TextExtension.Text);
            _LabelFilePathWarning.Visible   = string.IsNullOrWhiteSpace(_TextFilePath.Text);
            _LabelIconIndexWarning.Visible  = (!int.TryParse(_TextIconIndex.Text, out int _));
        }


        /// <summary>Item validating.</summary>
        private void _Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = (_LabelIdError.Visible || _LabelNameError.Visible);
        }
    }
}
