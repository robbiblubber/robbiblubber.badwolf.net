﻿using System;
using System.Collections.Generic;
using System.IO;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a folder base implementation.</summary>
    public abstract class Folder: Item, IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public Folder(Project project, DdpSection sec = null): base(project, sec)
        {
            if(sec != null)
            {
                CopyJob = sec.GetString("copyjob");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the items to copy.</summary>
        protected string[] _CopyItems
        {
            get
            {
                List<string> rval = new List<string>();
                foreach(string i in CopyJob.Replace("\r\n", "\n").Replace("\r", "\n").Replace(",", ";").Split(';'))
                {
                    if(string.IsNullOrWhiteSpace(i)) continue;
                    rval.Add(i.Trim());
                }

                return rval.ToArray();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Performs the copy operation.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        protected LogLevel _Copy(ICompiler compiler)
        {
            if(_CopyItems.Length == 0)
            {
                compiler.GiveFeedback(LogLevel.WARNING, "[" + ID + "] \"" + Name + "\" has an empty copy job.");
                return LogLevel.WARNING;
            }

            LogLevel rval = LogLevel.TRACE;
            string target = ((this is LocaleItem) ? compiler.RootDirectory : compiler.TargetDirectory);

            foreach(string i in _CopyItems)
            {
                try
                {
                    if(Directory.Exists(i))
                    {
                        compiler.GiveFeedback(LogLevel.TRACE, "Copying directory: \"" + i + "\".");
                        FileOp.CopyDirectory(i, target + @"\" + ID + @"\" + Path.GetFileName(i));
                    }
                    else
                    {
                        compiler.GiveFeedback(LogLevel.TRACE, "Copying files: \"" + i + "\".");
                        FileOp.CopyFiles(i, target + @"\" + ID);
                    }
                }
                catch(Exception ex)
                {
                    compiler.GiveFeedback(LogLevel.ERROR, "Failed to copy \"" + i + "\".");
                    compiler.GiveFeedback(LogLevel.DEBUG, "Error message [" + ID + "] \"" + Name + "\": " + ex.Message + ".", ex.ToString());
                    rval = LogLevel.ERROR;
                }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the copy job for this folder.</summary>
        public virtual string CopyJob
        {
            get; set;
        }


        /// <summary>Gets or sets if the folder has a target.</summary>
        public virtual bool HasTarget
        {
            get { return false; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            base.Save(parent);
            parent.SetString(ID + "/copyjob", CopyJob);
        }
    }
}
