﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a compiler.</summary>
    public class Compiler: ICompiler, ICompilerHandle
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current target directory.</summary>
        protected string _TargetDirectory;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Compiler()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="outputDirectory">Output directory.</param>
        public Compiler(string outputDirectory)
        {
            _TargetDirectory = OutputDirectory = outputDirectory;
        }



        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source item.</param>
        /// <param name="outputDirectory">Output directory.</param>
        public Compiler(IItem source, string outputDirectory)
        {
            Source = source;
            _TargetDirectory = OutputDirectory = outputDirectory;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // events                                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when feedback was received.</summary>
        public event FeedbackEventHandler Feedback;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the compiler source.</summary>
        public IItem Source
        {
            get; set; 
        }


        /// <summary>Gets or sets the output directory.</summary>
        public string OutputDirectory
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compiles the source.</summary>
        public void Compile()
        {
            Source.Compile(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICompiler                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the compilation root directory.</summary>
        string ICompiler.RootDirectory
        {
            get { return OutputDirectory; }
        }


        /// <summary>Gets the compilation target directory.</summary>
        string ICompiler.TargetDirectory 
        { 
            get { return _TargetDirectory; }
        }


        /// <summary>Allows components to give feedback.</summary>
        /// <param name="level">Feedback log level.</param>
        /// <param name="message">Feedback message.</param>
        /// <param name="data">Additional feedback data.</param>
        public void GiveFeedback(LogLevel level, string message, string data = null)
        {
            Feedback?.Invoke(this, new FeedbackEventArgs(level, message, data));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICompilerHandle                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the target directory.</summary>
        /// <param name="dir">Directory.</param>
        void ICompilerHandle.SetTargetDirectory(string dir)
        {
            _TargetDirectory = dir;
        }
    }
}
