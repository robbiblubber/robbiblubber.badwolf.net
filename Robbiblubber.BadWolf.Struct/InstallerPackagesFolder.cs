﻿using System;
using System.Collections;
using System.Collections.Generic;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents the installer packages folder.</summary>
    public class InstallerPackagesFolder: InstallerPackageParent, IInstallerPackageParent, IInstallerChild, IEnumerable<InstallerPackage>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Parent installer.</param>
        public InstallerPackagesFolder(Installer installer): base(installer)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the parent of an installer item.</summary>
        /// <param name="start">Starting parent.</param>
        /// <param name="item">Item.</param>
        /// <returns>Parent.</returns>
        private static IInstallerPackageParent _GetParent(IInstallerPackageParent start, InstallerPackage item)
        {

            if(start.Packages.Contains(item)) { return start; }

            IInstallerPackageParent rval;
            foreach(InstallerPackage i in start.Packages)
            {
                if((rval = _GetParent(i, item)) != null) { return rval; }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent for an installer package.</summary>
        /// <param name="item">Package item.</param>
        /// <returns>Parent.</returns>
        public IInstallerPackageParent GetParent(InstallerPackage item)
        {
            return _GetParent(this, item);
        }


        /// <summary>Loads installer settings data.</summary>
        /// <param name="sec">ddp Section.</param>
        public override void Load(DdpSection sec)
        {
            base.Load(sec.Sections["packages"]);
        }


        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            DdpSection sec = parent.Sections[Installer.ID].Sections.Add("packages");
            base.Save(sec);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<InstallerPackage>                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an enumerator for the list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<InstallerPackage> IEnumerable<InstallerPackage>.GetEnumerator()
        {
            return Packages.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for the list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Packages.GetEnumerator();
        }
    }
}
