﻿using System;

using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Container items implement this interface.</summary>
    public interface IContainerItem: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the items in this container.</summary>
        IMutableList<IItem> Items { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the item with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Item.</returns>
        IItem GetItem(string id);
    }
}
