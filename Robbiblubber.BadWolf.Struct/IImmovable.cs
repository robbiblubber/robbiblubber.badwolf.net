﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Immovable elements implement this interface.</summary>
    public interface IImmovable
    {}
}
