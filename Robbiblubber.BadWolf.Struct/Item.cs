﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a base implementation of the IItem interface.</summary>
    public abstract class Item: IItem, IShowable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp section.</param>
        public Item(Project project, DdpSection sec)
        {
            Project = project;

            if(sec == null)
            {
                ID = StringOp.Unique();
                Name = "Unnamed";
            }
            else
            {
                ID = sec.Name;
                Name = sec.GetString("name");
                Condition = sec.GetString("condition");
                Description = sec.GetString("description");
                SortIndex = sec.GetInteger("sort");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IShowable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent project.</summary>
        public Project Project { get; protected set; }


        /// <summary>Gets the showable item.</summary>
        public IItem GetItem()
        {
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the item ID.</summary>
        public virtual string ID { get; set; }


        /// <summary>Gets or sets the item name.</summary>
        public virtual string Name { get; set; }


        /// <summary>Gets or sets the item description.</summary>
        public virtual string Description { get; set; }


        /// <summary>Gets or sets the item condition.</summary>
        public virtual string Condition { get; set; }


        /// <summary>Gets or sets the item sort index.</summary>
        public int SortIndex { get; set; } = 200;


        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public virtual void Save(DdpSection parent)
        {
            parent.SetString(ID + "/name", Name);
            parent.SetString(ID + "/condition", Condition);
            parent.SetString(ID + "/description", Description);
            parent.SetInteger(ID + "/sort", SortIndex);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public abstract LogLevel Compile(ICompiler compiler);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines whether two object instances are equal.</summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>ReturnsTRUE if the specified object is equal to the current object, otherwise returns FALSE.</returns>
        public override bool Equals(object obj)
        {
            if(obj == null) { return false; }
            if(obj is IItem) { return (((IItem) obj).ID == ID); }

            return false;
        }


        /// <summary>Gets a hash value for this instance.</summary>
        /// <returns>Hash value.</returns>
        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // opertators                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Checks if two instances are equal.</summary>
        /// <param name="a">Instance.</param>
        /// <param name="b">Instance.</param>
        /// <returns>Returns TRUE if the instances are equal, otherwise returns FALSE.</returns>
        public static bool operator ==(Item a, IItem b)
        {
            if(((object) a) == null) { return (((object) b) == null); }
            if(((object) b) == null) { return false; }

            return (a.ID == b.ID);
        }


        /// <summary>Checks if two instances are not equal.</summary>
        /// <param name="a">Instance.</param>
        /// <param name="b">Instance.</param>
        /// <returns>Returns FALSE if the instances are equal, otherwise returns TRUE.</returns>
        public static bool operator !=(Item a, IItem b)
        {
            if(((object) a) == null) { return (((object) b) != null); }
            if(((object) b) == null) { return true; }

            return (a.ID != b.ID);
        }
    }
}
