﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class LinkControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkControl));
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LabelTargetLinkWarning = new System.Windows.Forms.Label();
            this._LabelFilePathWarning = new System.Windows.Forms.Label();
            this._LabelIconIndexWarning = new System.Windows.Forms.Label();
            this._TextNameLKey = new System.Windows.Forms.TextBox();
            this._LabelNameLKey = new System.Windows.Forms.Label();
            this._TextDecriptionLKey = new System.Windows.Forms.TextBox();
            this._LabelDecriptionLKey = new System.Windows.Forms.Label();
            this._LabelLocale = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelNameError = new System.Windows.Forms.Label();
            this._TextTargetLink = new System.Windows.Forms.TextBox();
            this._LabelTargetLink = new System.Windows.Forms.Label();
            this._TextFilePath = new System.Windows.Forms.TextBox();
            this._LabelFilePath = new System.Windows.Forms.Label();
            this._TextIconPath = new System.Windows.Forms.TextBox();
            this._LabelIconPath = new System.Windows.Forms.Label();
            this._TextIconIndex = new System.Windows.Forms.TextBox();
            this._LabelIconIndex = new System.Windows.Forms.Label();
            this._TextCondition = new System.Windows.Forms.TextBox();
            this._LabelConditionHint = new System.Windows.Forms.Label();
            this._LabelCondition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 73);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.Location = new System.Drawing.Point(30, 89);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            this._TextID.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(27, 376);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 9;
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(30, 392);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 9;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            this._TextName.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelTargetLinkWarning
            // 
            this._LabelTargetLinkWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTargetLinkWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTargetLinkWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelTargetLinkWarning.Image")));
            this._LabelTargetLinkWarning.Location = new System.Drawing.Point(7, 196);
            this._LabelTargetLinkWarning.Name = "_LabelTargetLinkWarning";
            this._LabelTargetLinkWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelTargetLinkWarning.TabIndex = 4;
            this._ToolTip.SetToolTip(this._LabelTargetLinkWarning, "Target link is empty");
            this._LabelTargetLinkWarning.Visible = false;
            // 
            // _LabelFilePathWarning
            // 
            this._LabelFilePathWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFilePathWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFilePathWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelFilePathWarning.Image")));
            this._LabelFilePathWarning.Location = new System.Drawing.Point(7, 242);
            this._LabelFilePathWarning.Name = "_LabelFilePathWarning";
            this._LabelFilePathWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelFilePathWarning.TabIndex = 5;
            this._ToolTip.SetToolTip(this._LabelFilePathWarning, "File path is empty");
            this._LabelFilePathWarning.Visible = false;
            // 
            // _LabelIconIndexWarning
            // 
            this._LabelIconIndexWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIconIndexWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIconIndexWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIconIndexWarning.Image")));
            this._LabelIconIndexWarning.Location = new System.Drawing.Point(7, 288);
            this._LabelIconIndexWarning.Name = "_LabelIconIndexWarning";
            this._LabelIconIndexWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelIconIndexWarning.TabIndex = 6;
            this._ToolTip.SetToolTip(this._LabelIconIndexWarning, "Icon index is invalid");
            this._LabelIconIndexWarning.Visible = false;
            // 
            // _TextNameLKey
            // 
            this._TextNameLKey.BackColor = System.Drawing.Color.AliceBlue;
            this._TextNameLKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextNameLKey.Location = new System.Drawing.Point(30, 143);
            this._TextNameLKey.Name = "_TextNameLKey";
            this._TextNameLKey.Size = new System.Drawing.Size(304, 25);
            this._TextNameLKey.TabIndex = 2;
            this._TextNameLKey.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelNameLKey
            // 
            this._LabelNameLKey.AutoSize = true;
            this._LabelNameLKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameLKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameLKey.Location = new System.Drawing.Point(27, 127);
            this._LabelNameLKey.Name = "_LabelNameLKey";
            this._LabelNameLKey.Size = new System.Drawing.Size(94, 13);
            this._LabelNameLKey.TabIndex = 2;
            this._LabelNameLKey.Text = "Name &Locale Key:";
            // 
            // _TextDecriptionLKey
            // 
            this._TextDecriptionLKey.BackColor = System.Drawing.Color.AliceBlue;
            this._TextDecriptionLKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDecriptionLKey.Location = new System.Drawing.Point(348, 143);
            this._TextDecriptionLKey.Name = "_TextDecriptionLKey";
            this._TextDecriptionLKey.Size = new System.Drawing.Size(304, 25);
            this._TextDecriptionLKey.TabIndex = 3;
            this._TextDecriptionLKey.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelDecriptionLKey
            // 
            this._LabelDecriptionLKey.AutoSize = true;
            this._LabelDecriptionLKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDecriptionLKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDecriptionLKey.Location = new System.Drawing.Point(345, 127);
            this._LabelDecriptionLKey.Name = "_LabelDecriptionLKey";
            this._LabelDecriptionLKey.Size = new System.Drawing.Size(124, 13);
            this._LabelDecriptionLKey.TabIndex = 3;
            this._LabelDecriptionLKey.Text = "Description Locale &Key:";
            // 
            // _LabelLocale
            // 
            this._LabelLocale.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLocale.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLocale.Image = ((System.Drawing.Image)(resources.GetObject("_LabelLocale.Image")));
            this._LabelLocale.Location = new System.Drawing.Point(7, 143);
            this._LabelLocale.Name = "_LabelLocale";
            this._LabelLocale.Size = new System.Drawing.Size(25, 25);
            this._LabelLocale.TabIndex = 2;
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(9, 89);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 1;
            this._LabelIdError.Visible = false;
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(9, 43);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._LabelNameError.Visible = false;
            // 
            // _TextTargetLink
            // 
            this._TextTargetLink.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextTargetLink.Location = new System.Drawing.Point(30, 196);
            this._TextTargetLink.Name = "_TextTargetLink";
            this._TextTargetLink.Size = new System.Drawing.Size(622, 25);
            this._TextTargetLink.TabIndex = 4;
            this._TextTargetLink.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelTargetLink
            // 
            this._LabelTargetLink.AutoSize = true;
            this._LabelTargetLink.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTargetLink.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTargetLink.Location = new System.Drawing.Point(27, 180);
            this._LabelTargetLink.Name = "_LabelTargetLink";
            this._LabelTargetLink.Size = new System.Drawing.Size(66, 13);
            this._LabelTargetLink.TabIndex = 4;
            this._LabelTargetLink.Text = "&Target Link:";
            // 
            // _TextFilePath
            // 
            this._TextFilePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextFilePath.Location = new System.Drawing.Point(30, 242);
            this._TextFilePath.Name = "_TextFilePath";
            this._TextFilePath.Size = new System.Drawing.Size(622, 25);
            this._TextFilePath.TabIndex = 5;
            this._TextFilePath.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelFilePath
            // 
            this._LabelFilePath.AutoSize = true;
            this._LabelFilePath.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFilePath.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFilePath.Location = new System.Drawing.Point(27, 226);
            this._LabelFilePath.Name = "_LabelFilePath";
            this._LabelFilePath.Size = new System.Drawing.Size(54, 13);
            this._LabelFilePath.TabIndex = 5;
            this._LabelFilePath.Text = "&File Path:";
            // 
            // _TextIconPath
            // 
            this._TextIconPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextIconPath.Location = new System.Drawing.Point(30, 288);
            this._TextIconPath.Name = "_TextIconPath";
            this._TextIconPath.Size = new System.Drawing.Size(501, 25);
            this._TextIconPath.TabIndex = 6;
            this._TextIconPath.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelIconPath
            // 
            this._LabelIconPath.AutoSize = true;
            this._LabelIconPath.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIconPath.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIconPath.Location = new System.Drawing.Point(27, 272);
            this._LabelIconPath.Name = "_LabelIconPath";
            this._LabelIconPath.Size = new System.Drawing.Size(58, 13);
            this._LabelIconPath.TabIndex = 6;
            this._LabelIconPath.Text = "&Icon Path:";
            // 
            // _TextIconIndex
            // 
            this._TextIconIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextIconIndex.Location = new System.Drawing.Point(537, 288);
            this._TextIconIndex.Name = "_TextIconIndex";
            this._TextIconIndex.Size = new System.Drawing.Size(115, 25);
            this._TextIconIndex.TabIndex = 7;
            this._TextIconIndex.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelIconIndex
            // 
            this._LabelIconIndex.AutoSize = true;
            this._LabelIconIndex.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIconIndex.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIconIndex.Location = new System.Drawing.Point(534, 272);
            this._LabelIconIndex.Name = "_LabelIconIndex";
            this._LabelIconIndex.Size = new System.Drawing.Size(63, 13);
            this._LabelIconIndex.TabIndex = 7;
            this._LabelIconIndex.Text = "Icon Inde&x:";
            // 
            // _TextCondition
            // 
            this._TextCondition.BackColor = System.Drawing.Color.MintCream;
            this._TextCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCondition.Location = new System.Drawing.Point(30, 340);
            this._TextCondition.Name = "_TextCondition";
            this._TextCondition.Size = new System.Drawing.Size(622, 25);
            this._TextCondition.TabIndex = 8;
            this._TextCondition.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelConditionHint
            // 
            this._LabelConditionHint.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelConditionHint.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelConditionHint.Image = global::Robbiblubber.BadWolf.Struct.Resources.hash;
            this._LabelConditionHint.Location = new System.Drawing.Point(9, 340);
            this._LabelConditionHint.Name = "_LabelConditionHint";
            this._LabelConditionHint.Size = new System.Drawing.Size(25, 25);
            this._LabelConditionHint.TabIndex = 8;
            this._LabelConditionHint.Visible = false;
            // 
            // _LabelCondition
            // 
            this._LabelCondition.AutoSize = true;
            this._LabelCondition.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCondition.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCondition.Location = new System.Drawing.Point(27, 324);
            this._LabelCondition.Name = "_LabelCondition";
            this._LabelCondition.Size = new System.Drawing.Size(62, 13);
            this._LabelCondition.TabIndex = 8;
            this._LabelCondition.Text = "&Condition:";
            // 
            // LinkControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._TextCondition);
            this.Controls.Add(this._LabelConditionHint);
            this.Controls.Add(this._LabelCondition);
            this.Controls.Add(this._TextIconPath);
            this.Controls.Add(this._LabelIconIndexWarning);
            this.Controls.Add(this._TextIconIndex);
            this.Controls.Add(this._LabelIconIndex);
            this.Controls.Add(this._LabelIconPath);
            this.Controls.Add(this._TextFilePath);
            this.Controls.Add(this._LabelFilePathWarning);
            this.Controls.Add(this._LabelFilePath);
            this.Controls.Add(this._TextTargetLink);
            this.Controls.Add(this._LabelTargetLinkWarning);
            this.Controls.Add(this._LabelTargetLink);
            this.Controls.Add(this._TextDecriptionLKey);
            this.Controls.Add(this._LabelDecriptionLKey);
            this.Controls.Add(this._TextNameLKey);
            this.Controls.Add(this._LabelLocale);
            this.Controls.Add(this._LabelNameLKey);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LinkControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.TextBox _TextNameLKey;
        private System.Windows.Forms.Label _LabelLocale;
        private System.Windows.Forms.Label _LabelNameLKey;
        private System.Windows.Forms.TextBox _TextDecriptionLKey;
        private System.Windows.Forms.Label _LabelDecriptionLKey;
        private System.Windows.Forms.TextBox _TextTargetLink;
        private System.Windows.Forms.Label _LabelTargetLinkWarning;
        private System.Windows.Forms.Label _LabelTargetLink;
        private System.Windows.Forms.TextBox _TextFilePath;
        private System.Windows.Forms.Label _LabelFilePathWarning;
        private System.Windows.Forms.Label _LabelFilePath;
        private System.Windows.Forms.TextBox _TextIconPath;
        private System.Windows.Forms.Label _LabelIconPath;
        private System.Windows.Forms.TextBox _TextIconIndex;
        private System.Windows.Forms.Label _LabelIconIndex;
        private System.Windows.Forms.Label _LabelIconIndexWarning;
        private System.Windows.Forms.TextBox _TextCondition;
        private System.Windows.Forms.Label _LabelConditionHint;
        private System.Windows.Forms.Label _LabelCondition;
    }
}
