﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This enumeration defines log levels.</summary>
    public enum LogLevel: int
    {
        /// <summary>Full trace log entries.</summary>
        TRACE = 0,
        /// <summary>Debug log entries.</summary>
        DEBUG = 1,
        /// <summary>Information log entries.</summary>
        INFORMATION = 2,
        /// <summary>Warning log entries.</summary>
        WARNING = 3,
        /// <summary>Error log entries.</summary>
        ERROR = 4,
        /// <summary>Critical log entries.</summary>
        CRITICAL = 5
    }
}
