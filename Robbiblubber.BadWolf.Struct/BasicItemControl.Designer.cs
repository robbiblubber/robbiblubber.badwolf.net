﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class BasicItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BasicItemControl));
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._TextNameLKey = new System.Windows.Forms.TextBox();
            this._LabelNameLKey = new System.Windows.Forms.Label();
            this._TextDecriptionLKey = new System.Windows.Forms.TextBox();
            this._LabelDescriptionLKey = new System.Windows.Forms.Label();
            this._LabelLocaleHint = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelNameError = new System.Windows.Forms.Label();
            this._TextCondition = new System.Windows.Forms.TextBox();
            this._LabelConditionHint = new System.Windows.Forms.Label();
            this._LabelCondition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 75);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.Location = new System.Drawing.Point(30, 91);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            this._TextID.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(27, 241);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 5;
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(30, 257);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 5;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            this._TextName.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _TextNameLKey
            // 
            this._TextNameLKey.BackColor = System.Drawing.Color.AliceBlue;
            this._TextNameLKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextNameLKey.Location = new System.Drawing.Point(30, 147);
            this._TextNameLKey.Name = "_TextNameLKey";
            this._TextNameLKey.Size = new System.Drawing.Size(304, 25);
            this._TextNameLKey.TabIndex = 2;
            this._TextNameLKey.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelNameLKey
            // 
            this._LabelNameLKey.AutoSize = true;
            this._LabelNameLKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameLKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameLKey.Location = new System.Drawing.Point(27, 131);
            this._LabelNameLKey.Name = "_LabelNameLKey";
            this._LabelNameLKey.Size = new System.Drawing.Size(94, 13);
            this._LabelNameLKey.TabIndex = 2;
            this._LabelNameLKey.Text = "Name &Locale Key:";
            // 
            // _TextDecriptionLKey
            // 
            this._TextDecriptionLKey.BackColor = System.Drawing.Color.AliceBlue;
            this._TextDecriptionLKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDecriptionLKey.Location = new System.Drawing.Point(348, 147);
            this._TextDecriptionLKey.Name = "_TextDecriptionLKey";
            this._TextDecriptionLKey.Size = new System.Drawing.Size(304, 25);
            this._TextDecriptionLKey.TabIndex = 3;
            this._TextDecriptionLKey.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelDescriptionLKey
            // 
            this._LabelDescriptionLKey.AutoSize = true;
            this._LabelDescriptionLKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescriptionLKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescriptionLKey.Location = new System.Drawing.Point(345, 131);
            this._LabelDescriptionLKey.Name = "_LabelDescriptionLKey";
            this._LabelDescriptionLKey.Size = new System.Drawing.Size(124, 13);
            this._LabelDescriptionLKey.TabIndex = 3;
            this._LabelDescriptionLKey.Text = "Description Locale &Key:";
            // 
            // _LabelLocaleHint
            // 
            this._LabelLocaleHint.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLocaleHint.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLocaleHint.Image = ((System.Drawing.Image)(resources.GetObject("_LabelLocaleHint.Image")));
            this._LabelLocaleHint.Location = new System.Drawing.Point(7, 147);
            this._LabelLocaleHint.Name = "_LabelLocaleHint";
            this._LabelLocaleHint.Size = new System.Drawing.Size(25, 25);
            this._LabelLocaleHint.TabIndex = 2;
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(9, 91);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 1;
            this._LabelIdError.Visible = false;
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(9, 43);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._LabelNameError.Visible = false;
            // 
            // _TextCondition
            // 
            this._TextCondition.BackColor = System.Drawing.Color.MintCream;
            this._TextCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCondition.Location = new System.Drawing.Point(30, 201);
            this._TextCondition.Name = "_TextCondition";
            this._TextCondition.Size = new System.Drawing.Size(622, 25);
            this._TextCondition.TabIndex = 4;
            this._TextCondition.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelConditionHint
            // 
            this._LabelConditionHint.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelConditionHint.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelConditionHint.Image = global::Robbiblubber.BadWolf.Struct.Resources.hash;
            this._LabelConditionHint.Location = new System.Drawing.Point(9, 201);
            this._LabelConditionHint.Name = "_LabelConditionHint";
            this._LabelConditionHint.Size = new System.Drawing.Size(25, 25);
            this._LabelConditionHint.TabIndex = 4;
            this._LabelConditionHint.Visible = false;
            // 
            // _LabelCondition
            // 
            this._LabelCondition.AutoSize = true;
            this._LabelCondition.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCondition.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCondition.Location = new System.Drawing.Point(27, 185);
            this._LabelCondition.Name = "_LabelCondition";
            this._LabelCondition.Size = new System.Drawing.Size(62, 13);
            this._LabelCondition.TabIndex = 4;
            this._LabelCondition.Text = "&Condition:";
            // 
            // BasicItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._TextCondition);
            this.Controls.Add(this._LabelConditionHint);
            this.Controls.Add(this._LabelCondition);
            this.Controls.Add(this._TextDecriptionLKey);
            this.Controls.Add(this._LabelDescriptionLKey);
            this.Controls.Add(this._TextNameLKey);
            this.Controls.Add(this._LabelLocaleHint);
            this.Controls.Add(this._LabelNameLKey);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BasicItemControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.TextBox _TextNameLKey;
        private System.Windows.Forms.Label _LabelLocaleHint;
        private System.Windows.Forms.Label _LabelNameLKey;
        private System.Windows.Forms.TextBox _TextDecriptionLKey;
        private System.Windows.Forms.Label _LabelDescriptionLKey;
        private System.Windows.Forms.TextBox _TextCondition;
        private System.Windows.Forms.Label _LabelConditionHint;
        private System.Windows.Forms.Label _LabelCondition;
    }
}
