﻿using System;
using System.IO;
using System.Linq;

using Robbiblubber.BadWolf.Struct.Exceptions;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a Bad Wolf Package.</summary>
    public class Package: Item, IItem, IContainerItem, ILocalizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saved project source.</summary>
        private string _SavedSource;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="fileName">File name.</param>
        public Package(Project project, string fileName): base(project, null)
        {
            Ddp ddp = Ddp.Load(fileName);
            DdpSection sec = null;

            if(ddp.GetString("file") != "bwpkg") throw new LoadException("File is invalid.");
            CommonStandards.CheckVersion(ddp);

            foreach(DdpSection i in ddp.Sections)
            {
                if(i.GetString("type") == "package") { sec = i; break; }
            }

            if(sec == null) throw new LoadException("File is invalid.");

            FileName = fileName;
            ID = sec.Name;
            Name = sec.GetString("name");
            Description = sec.GetString("description");

            NameLocaleKey = sec.GetString("lk.name");
            DescriptionLocaleKey = sec.GetString("lk.description");

            foreach(DdpSection i in sec.Sections)
            {
                IItem item = i.LoadItem(Project);
                if(item != null) { Items.Add(item); }
            }

            _SavedSource = _GetDdp().Text;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="id">Package ID.</param>
        /// <param name="name">Package name.</param>
        /// <param name="description">Package description.</param>
        public Package(Project project, string fileName, string id, string name, string description): base(project, null)
        {
            FileName = fileName;
            ID = id;
            Name = name;
            Description = description;

            FileName = fileName;
            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the project ddp.</summary>
        /// <returns>ddp.</returns>
        private Ddp _GetDdp()
        {
            Ddp rval = new Ddp();
            rval.SetString("file", "bwpkg");
            rval.SetString("version", VersionOp.ApplicationVersion.ToVersionString());

            Save(rval);
            DdpSection p = rval.Sections[ID];

            foreach(IItem i in Items) { i.Save(p); }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the project file name.</summary>
        public string FileName { get; }
        

        /// <summary>Gets if the project is used in installers of this project.</summary>
        public bool UsedInInstllers
        {
            get { return (Project.GetInstallerPackage(ID) != null); }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        public void Save()
        {
            _SavedSource = _GetDdp().Text;
            File.WriteAllText(FileName, _SavedSource);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IContainerItem                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items in this package.</summary>
        public IMutableList<IItem> Items 
        { 
            get; 
        } = new ItemList<IItem>();


        /// <summary>Returns the item with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Item.</returns>
        public IItem GetItem(string id)
        {
            if(ID == id) { return this; }

            foreach(IItem i in Items)
            {
                if(i.ID == id) return i;
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILocalizable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the name locale key.</summary>
        public string NameLocaleKey
        {
            get; set;
        }


        /// <summary>Gets or sets the description locale key.</summary>
        public string DescriptionLocaleKey
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "package");
            base.Save(parent);

            parent.SetString(ID + "/lk.name", NameLocaleKey);
            parent.SetString(ID + "/lk.description", DescriptionLocaleKey);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            compiler.GiveFeedback(LogLevel.INFORMATION, "Compiling package [" + ID + "] \"" + Name + "\".");
            LogLevel rval = LogLevel.INFORMATION;
            LogLevel result = LogLevel.ERROR;

            Ddp cfg = new Ddp();
            cfg.SetString("id", ID);
            cfg.SetString("name", Name);
            cfg.SetString("description", Description);
            cfg.SetString("lk.name", NameLocaleKey);
            cfg.SetString("lk.description", DescriptionLocaleKey);

            int n = 0;
            foreach(IItem i in Items.OrderBy(m => m.SortIndex))
            {
                cfg.SetString("items/[" + n.ToString() + "]/id", i.ID);
                cfg.SetString("items/[" + n.ToString() + "]/type", i.GetController().TypeKey);
                n++;

                try
                {
                    compiler.GiveFeedback(LogLevel.TRACE, "Calling compile on [" + i.ID + "] \"" + i.Name + "\".");
                    result = i.Compile(compiler);
                    compiler.GiveFeedback(LogLevel.TRACE, "Returned from compile on [" + i.ID + "] \"" + i.Name + "\".");
                }
                catch(Exception ex) 
                {
                    compiler.GiveFeedback(LogLevel.CRITICAL, "Compilation of [" + i.ID + "] \"" + i.Name + "\" has terminated with errors.");
                    compiler.GiveFeedback(LogLevel.DEBUG, "Error message [" + i.ID + "] \"" + i.Name + "\": " + ex.Message + ".", ex.ToString());
                    rval = LogLevel.CRITICAL;
                }
            }

            compiler.GiveFeedback(LogLevel.TRACE, "Creating package configuration [" + ID + "] \"" + Name + "\".");
            cfg.Save(compiler.RootDirectory + @"\" + ID + ".package");

            string msg = "";
            if(result == LogLevel.WARNING)
            {
                msg = " with warnings";
            }
            else if(result > LogLevel.WARNING) { msg = " with errors"; }

            compiler.GiveFeedback(LogLevel.TRACE, "Completed comilation of package [" + ID + "] \"" + Name + "\"" + msg + ".");

            return ((result > rval) ? result : rval);
        }
    }
}
