﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Localizable items implement this interface.</summary>
    public interface ILocalizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the name locale key.</summary>
        string NameLocaleKey { get; set; }


        /// <summary>Gets or sets the description locale key.</summary>
        string DescriptionLocaleKey { get; set; }
    }
}
