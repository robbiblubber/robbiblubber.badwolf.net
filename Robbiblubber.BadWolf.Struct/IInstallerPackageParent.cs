﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Objects that contain installer packages implement this interface.</summary>
    public interface IInstallerPackageParent
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the child packages of this item.</summary>
        IMutableList<InstallerPackage> Packages { get; }


        /// <summary>Gets all child packages of this item.</summary>
        IImmutableList<Package> AllChildPackages { get; }
    }
}
