﻿using Robbiblubber.Util.Library;
using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Item base interface.</summary>
    public interface IItem: IShowable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the item ID.</summary>
        string ID { get; set; }


        /// <summary>Gets or sets the item name.</summary>
        string Name { get; set; }


        /// <summary>Gets or sets the item description.</summary>
        string Description { get; set; }


        /// <summary>Gets or sets the item condition.</summary>
        string Condition { get; set; }


        /// <summary>Gets or sets the item sort index.</summary>
        int SortIndex { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        void Save(DdpSection parent);


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        LogLevel Compile(ICompiler compiler);
    }
}
