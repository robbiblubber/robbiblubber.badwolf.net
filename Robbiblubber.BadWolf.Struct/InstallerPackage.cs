﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements an installer package.</summary>
    public class InstallerPackage: InstallerPackageParent, IInstallerPackageParent, IItem, IShowable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Package ID.</summary>
        private string _ID = null;

        /// <summary>Package file name.</summary>
        private string _FileName = null;

        /// <summary>Sort index.</summary>
        private int _SortIndex = -1024;

        /// <summary>Package.</summary>
        private Package _Package = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Installer.</param>
        /// <param name="package">Package.</param>
        public InstallerPackage(Installer installer, string packageID): base(installer)
        {
            _ID = packageID;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Installer.</param>
        /// <param name="package">Package.</param>
        public InstallerPackage(Installer installer, Package package): this(installer, package.ID)
        {
            _Package = package;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Installer.</param>
        /// <param name="sec">ddp Section.</param>
        public InstallerPackage(Installer installer, DdpSection sec): this(installer, sec.Name)
        {
            Load(sec);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the package is visible.</summary>
        /// <remarks>Visible packages are displayed in the installer package selection step.</remarks>
        public bool Visible
        {
            get; set;
        } = true;


        /// <summary>Gets or sets if the package is selectable.</summary>
        /// <remarks>Selectable packages can be selected in the installer package selection step.</remarks>
        public bool Selectable
        {
            get; set;
        } = true;


        /// <summary>Gets or sets if the package is selected.</summary>
        public bool Selected
        {
            get; set;
        } = true;
        

        /// <summary>Gets the installer package.</summary>
        public Package Package
        {
            get
            {
                if(_Package == null) 
                { 
                    _Package = Project.Packages[_ID];
                    if(_Package == null)
                    {
                        _Package = new Package(Installer.Project, _FileName);
                    }

                }
                return _Package;
            }
        }


        /// <summary>Gets this object's parent.</summary>
        public IInstallerPackageParent Parent
        {
            get { return Installer.Packages.GetParent(this); }
        }


        /// <summary>Gets this object's siblings.</summary>
        public IImmutableList<InstallerPackage> Siblings
        {
            get { return Parent.Packages; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Removes the installer package from the installer.</summary>
        public void Remove()
        {
            Parent.Packages.Remove(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] InstallerPackageParent                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the showable item.</summary>
        public override IItem GetItem()
        {
            return this;
        }


        /// <summary>Loads installer settings data.</summary>
        /// <param name="sec">ddp Section.</param>
        public override void Load(DdpSection sec)
        {
            _ID = sec.Name;
            _FileName = Path.GetDirectoryName(Project.FileName) + '\\' + sec.GetString("file");
            Visible = sec.GetBoolean("visible");
            Selectable = sec.GetBoolean("selectable");
            Selected = sec.GetBoolean("selected");
            SortIndex = sec.GetInteger("sort", -1024);

            base.Load(sec);
        }


        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            DdpSection sec = parent.Sections.Add(ID);
            sec.SetBoolean("visible", Visible);
            sec.SetBoolean("selectable", Selectable);
            sec.SetBoolean("selected", Selected);
            string aa = FileOp.GetRelativePath(Path.GetDirectoryName(Project.FileName), _FileName);
            sec.SetString("file", FileOp.GetRelativePath(Path.GetDirectoryName(Project.FileName), _FileName));
            sec.SetInteger("sort", SortIndex);

            base.Save(sec);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the item ID.</summary>
        public string ID 
        { 
            get { return _ID; }
            set { throw new Exception("Unable to set ID for installer package."); }
        }


        /// <summary>Gets or sets the item name.</summary>
        public string Name 
        { 
            get { return Package.Name; }
            set { throw new Exception("Unable to set name for installer package."); }
        }


        /// <summary>Gets or sets the item condition.</summary>
        public string Condition
        {
            get { return Package.Condition; }
            set { throw new Exception("Unable to set condition for installer package."); }
        }


        /// <summary>Gets or sets the item description.</summary>
        public string Description 
        { 
            get { return Package.Description; }
            set { throw new Exception("Unable to set description for installer package."); }
        }


        /// <summary>Gets or sets the item sort index.</summary>
        public int SortIndex
        {
            get
            {
                if(_SortIndex == -1024) 
                { 
                    _SortIndex = (Siblings.Max(m => m._SortIndex) + 1); 
                    if(_SortIndex < 0) { _SortIndex = 10; }
                }

                return _SortIndex;
            }
            set { _SortIndex = value; }
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public LogLevel Compile(ICompiler compiler)
        {
            // TODO: compile installer package...
            return LogLevel.TRACE;
        }
    }
}
