﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements a payload folder.</summary>
    public class PayloadFolder: Folder, IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public PayloadFolder(Project project, DdpSection sec = null) : base(project, sec)
        {
            if(sec != null)
            {
                Target = sec.GetString("target");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the folder has a target.</summary>
        public override bool HasTarget
        {
            get { return true; }
        }


        /// <summary>Gets the target path for this folder.</summary>
        public virtual string Target
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Folder                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the folder.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "folder:payload");
            base.Save(parent);
            parent.SetString(ID + "/target", Target);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            compiler.GiveFeedback(LogLevel.TRACE, "Compiling payload folder [" + ID + "] \"" + Name + "\".");
            LogLevel rval = _Copy(compiler);

            compiler.GiveFeedback(LogLevel.TRACE, "Creating payload folder configuration [" + ID + "] \"" + Name + "\".");
            Ddp cfg = new Ddp();

            cfg.SetString("id", ID);
            cfg.SetString("type", "folder:payload");
            cfg.SetString("name", Name);
            cfg.SetString("target", Target);
            cfg.Save(compiler.TargetDirectory + @"\" + ID + ".config");

            return rval;
        }
    }
}