﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class FolderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderControl));
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelTarget = new System.Windows.Forms.Label();
            this._TextTarget = new System.Windows.Forms.TextBox();
            this._LabelTargetWarning = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LabelNameError = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelCopyWarning = new System.Windows.Forms.Label();
            this._PanelSouth = new System.Windows.Forms.Panel();
            this._LabelCondition = new System.Windows.Forms.Label();
            this._TextCondition = new System.Windows.Forms.TextBox();
            this._LabelConditionHint = new System.Windows.Forms.Label();
            this._TextCopy = new System.Windows.Forms.TextBox();
            this._LabelCopy = new System.Windows.Forms.Label();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._PanelSouth.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 73);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.Location = new System.Drawing.Point(30, 89);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            this._TextID.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            this._TextName.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelTarget
            // 
            this._LabelTarget.AutoSize = true;
            this._LabelTarget.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTarget.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTarget.Location = new System.Drawing.Point(27, 124);
            this._LabelTarget.Name = "_LabelTarget";
            this._LabelTarget.Size = new System.Drawing.Size(42, 13);
            this._LabelTarget.TabIndex = 2;
            this._LabelTarget.Text = "&Target:";
            // 
            // _TextTarget
            // 
            this._TextTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextTarget.Location = new System.Drawing.Point(30, 140);
            this._TextTarget.Name = "_TextTarget";
            this._TextTarget.Size = new System.Drawing.Size(622, 25);
            this._TextTarget.TabIndex = 2;
            this._TextTarget.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelTargetWarning
            // 
            this._LabelTargetWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTargetWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTargetWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelTargetWarning.Image")));
            this._LabelTargetWarning.Location = new System.Drawing.Point(7, 140);
            this._LabelTargetWarning.Name = "_LabelTargetWarning";
            this._LabelTargetWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelTargetWarning.TabIndex = 2;
            this._ToolTip.SetToolTip(this._LabelTargetWarning, "Target is empty");
            this._LabelTargetWarning.Visible = false;
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(7, 43);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._ToolTip.SetToolTip(this._LabelNameError, "Name is empty");
            this._LabelNameError.Visible = false;
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(7, 89);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 7;
            this._ToolTip.SetToolTip(this._LabelIdError, "ID is empty");
            this._LabelIdError.Visible = false;
            // 
            // _LabelCopyWarning
            // 
            this._LabelCopyWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCopyWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCopyWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelCopyWarning.Image")));
            this._LabelCopyWarning.Location = new System.Drawing.Point(4, 21);
            this._LabelCopyWarning.Name = "_LabelCopyWarning";
            this._LabelCopyWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelCopyWarning.TabIndex = 3;
            this._ToolTip.SetToolTip(this._LabelCopyWarning, "Copy job is empty");
            this._LabelCopyWarning.Visible = false;
            // 
            // _PanelSouth
            // 
            this._PanelSouth.Controls.Add(this._LabelCondition);
            this._PanelSouth.Controls.Add(this._TextCondition);
            this._PanelSouth.Controls.Add(this._LabelConditionHint);
            this._PanelSouth.Controls.Add(this._TextCopy);
            this._PanelSouth.Controls.Add(this._LabelCopyWarning);
            this._PanelSouth.Controls.Add(this._LabelCopy);
            this._PanelSouth.Controls.Add(this._LabelDescription);
            this._PanelSouth.Controls.Add(this._TextDescription);
            this._PanelSouth.Location = new System.Drawing.Point(3, 171);
            this._PanelSouth.Name = "_PanelSouth";
            this._PanelSouth.Size = new System.Drawing.Size(682, 327);
            this._PanelSouth.TabIndex = 3;
            // 
            // _LabelCondition
            // 
            this._LabelCondition.AutoSize = true;
            this._LabelCondition.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCondition.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCondition.Location = new System.Drawing.Point(24, 144);
            this._LabelCondition.Name = "_LabelCondition";
            this._LabelCondition.Size = new System.Drawing.Size(62, 13);
            this._LabelCondition.TabIndex = 4;
            this._LabelCondition.Text = "Condition:";
            // 
            // _TextCondition
            // 
            this._TextCondition.BackColor = System.Drawing.Color.MintCream;
            this._TextCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCondition.Location = new System.Drawing.Point(27, 160);
            this._TextCondition.Name = "_TextCondition";
            this._TextCondition.Size = new System.Drawing.Size(622, 25);
            this._TextCondition.TabIndex = 4;
            this._TextCondition.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelConditionHint
            // 
            this._LabelConditionHint.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelConditionHint.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelConditionHint.Image = global::Robbiblubber.BadWolf.Struct.Resources.hash;
            this._LabelConditionHint.Location = new System.Drawing.Point(6, 160);
            this._LabelConditionHint.Name = "_LabelConditionHint";
            this._LabelConditionHint.Size = new System.Drawing.Size(25, 25);
            this._LabelConditionHint.TabIndex = 4;
            this._LabelConditionHint.Visible = false;
            // 
            // _TextCopy
            // 
            this._TextCopy.BackColor = System.Drawing.SystemColors.Window;
            this._TextCopy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCopy.Location = new System.Drawing.Point(27, 21);
            this._TextCopy.Multiline = true;
            this._TextCopy.Name = "_TextCopy";
            this._TextCopy.Size = new System.Drawing.Size(622, 113);
            this._TextCopy.TabIndex = 3;
            this._TextCopy.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelCopy
            // 
            this._LabelCopy.AutoSize = true;
            this._LabelCopy.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCopy.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCopy.Location = new System.Drawing.Point(24, 5);
            this._LabelCopy.Name = "_LabelCopy";
            this._LabelCopy.Size = new System.Drawing.Size(62, 13);
            this._LabelCopy.TabIndex = 3;
            this._LabelCopy.Text = "&Copy Files:";
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(24, 196);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 5;
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(27, 212);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 5;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // FolderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._TextTarget);
            this.Controls.Add(this._PanelSouth);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelTargetWarning);
            this.Controls.Add(this._LabelTarget);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FolderControl";
            this.Size = new System.Drawing.Size(688, 505);
            this._PanelSouth.ResumeLayout(false);
            this._PanelSouth.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelTarget;
        private System.Windows.Forms.TextBox _TextTarget;
        private System.Windows.Forms.Label _LabelTargetWarning;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.Panel _PanelSouth;
        private System.Windows.Forms.TextBox _TextCopy;
        private System.Windows.Forms.Label _LabelCopyWarning;
        private System.Windows.Forms.Label _LabelCopy;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.TextBox _TextCondition;
        private System.Windows.Forms.Label _LabelConditionHint;
        private System.Windows.Forms.Label _LabelCondition;
    }
}
