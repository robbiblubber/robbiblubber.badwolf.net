﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Compilers implement this interface.</summary>
    public interface ICompiler
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the compilation root directory.</summary>
        string RootDirectory { get; }


        /// <summary>Gets the compilation target directory.</summary>
        string TargetDirectory { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Allows components to give feedback.</summary>
        /// <param name="level">Feedback log level.</param>
        /// <param name="message">Feedback message.</param>
        /// <param name="data">Additional feedback data.</param>
        void GiveFeedback(LogLevel level, string message, string data = null);
    }
}
