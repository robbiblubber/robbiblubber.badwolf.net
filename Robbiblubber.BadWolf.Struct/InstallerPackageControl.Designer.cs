﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class InstallerPackageControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._TextID = new System.Windows.Forms.TextBox();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelID = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._CheckVisible = new System.Windows.Forms.CheckBox();
            this._CheckSelectable = new System.Windows.Forms.CheckBox();
            this._CheckSelected = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _TextID
            // 
            this._TextID.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._TextID.Location = new System.Drawing.Point(30, 89);
            this._TextID.Name = "_TextID";
            this._TextID.ReadOnly = true;
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            // 
            // _TextName
            // 
            this._TextName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.ReadOnly = true;
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 73);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._TextDescription.Location = new System.Drawing.Point(30, 235);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.ReadOnly = true;
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 5;
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(27, 219);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 5;
            this._LabelDescription.Text = "&Description:";
            // 
            // _CheckVisible
            // 
            this._CheckVisible.AutoSize = true;
            this._CheckVisible.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckVisible.Location = new System.Drawing.Point(30, 131);
            this._CheckVisible.Name = "_CheckVisible";
            this._CheckVisible.Size = new System.Drawing.Size(132, 21);
            this._CheckVisible.TabIndex = 2;
            this._CheckVisible.Text = " Package is &visible";
            this._CheckVisible.UseVisualStyleBackColor = true;
            this._CheckVisible.CheckedChanged += new System.EventHandler(this._Changed);
            // 
            // _CheckSelectable
            // 
            this._CheckSelectable.AutoSize = true;
            this._CheckSelectable.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckSelectable.Location = new System.Drawing.Point(30, 158);
            this._CheckSelectable.Name = "_CheckSelectable";
            this._CheckSelectable.Size = new System.Drawing.Size(154, 21);
            this._CheckSelectable.TabIndex = 3;
            this._CheckSelectable.Text = " &Package is selectable";
            this._CheckSelectable.UseVisualStyleBackColor = true;
            this._CheckSelectable.CheckedChanged += new System.EventHandler(this._Changed);
            // 
            // _CheckSelected
            // 
            this._CheckSelected.AutoSize = true;
            this._CheckSelected.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckSelected.Location = new System.Drawing.Point(30, 185);
            this._CheckSelected.Name = "_CheckSelected";
            this._CheckSelected.Size = new System.Drawing.Size(144, 21);
            this._CheckSelected.TabIndex = 4;
            this._CheckSelected.Text = " Package is selected";
            this._CheckSelected.UseVisualStyleBackColor = true;
            this._CheckSelected.CheckedChanged += new System.EventHandler(this._Changed);
            // 
            // InstallerPackageControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._CheckSelected);
            this.Controls.Add(this._CheckSelectable);
            this.Controls.Add(this._CheckVisible);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "InstallerPackageControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.CheckBox _CheckVisible;
        private System.Windows.Forms.CheckBox _CheckSelectable;
        private System.Windows.Forms.CheckBox _CheckSelected;
    }
}
