﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a file association.</summary>
    public class FileAssociation: Item, IItem, ILocalizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public FileAssociation(Project project, DdpSection sec = null): base(project, sec)
        {
            if(sec != null)
            {
                Extension = sec.GetString("extension");
                FilePath = sec.GetString("filepath");
                IconPath = sec.GetString("iconpath");
                IconIndex = sec.GetInteger("iconindex");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the file extension.</summary>
        public string Extension
        {
            get; set;
        }


        /// <summary>Gets or sets the target file path.</summary>
        public string FilePath
        {
            get; set;
        }


        /// <summary>Gets or sets icon file.</summary>
        public string IconPath
        {
            get; set;
        }


        /// <summary>Gets or sets the icon index.</summary>
        public int IconIndex
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILocalizable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the name locale key.</summary>
        public string NameLocaleKey
        {
            get; set;
        }


        /// <summary>Gets or sets the description locale key.</summary>
        public string DescriptionLocaleKey
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "association");
            base.Save(parent);

            parent.SetString(ID + "/extension", Extension);
            parent.SetString(ID + "/filepath", FilePath);
            parent.SetString(ID + "/iconpath", IconPath);
            parent.SetInteger(ID + "/iconindex", IconIndex);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            LogLevel rval = LogLevel.TRACE;

            compiler.GiveFeedback(LogLevel.TRACE, "Creating file association configuration [" + ID + "] \"" + Name + "\".");
            Ddp cfg = new Ddp();
            Save(cfg);
            cfg.Save(compiler.TargetDirectory + @"\" + ID + ".config");

            return rval;
        }
    }
}
