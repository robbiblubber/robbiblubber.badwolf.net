﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This delegate provides an event handler for feedback events.</summary>
    /// <param name="sender">Sending object.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void FeedbackEventHandler(object sender, FeedbackEventArgs e);
}
