﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This enumeration defines execution levels.</summary>
    public enum ExecutionLevel
    {
        /// <summary>Use invoker privileges.</summary>
        AS_INVOKER = 0,
        /// <summary>Use administrator if possible.</summary>
        HIGHEST_AVAILABLE = 1,
        /// <summary>Requires administrator privileges.</summary>
        REQUIRE_ADMINISTRATOR = 2
    }
}
