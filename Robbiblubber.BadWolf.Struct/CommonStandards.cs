﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.BadWolf.Struct.Exceptions;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides common standards for the structure.</summary>
    public static class CommonStandards
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Minumum structure version.</summary>
        public static readonly Version MINIMUM_VERSION = new Version("2.0.0");

        /// <summary>Minumum structure version.</summary>
        public static readonly Version MAXIMUM_VERSION = new Version("3.3.0");



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the common working directory.</summary>
        public static string WorkingDirectory
        {
            get; set;
        } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if a version is supported by this structure version.</summary>
        /// <param name="v">Version.</param>
        /// <returns>Returns TRUE if the version is supported, otherwise returns FALSE.</returns>
        public static bool IsSupportedVersion(Version v)
        {
            return ((v >= MINIMUM_VERSION) && (v <= MAXIMUM_VERSION));
        }


        /// <summary>Gets if a version is supported by this structure version.</summary>
        /// <param name="sec">ddp section.</param>
        /// <returns>Returns TRUE if the version is supported, otherwise returns FALSE.</returns>
        public static bool IsSupportedVersion(DdpSection sec)
        {
            return IsSupportedVersion(new Version(sec.GetString("version")));
        }


        /// <summary>Checks if a file version is supported.</summary>
        /// <param name="sec">ddp section.</param>
        /// <exception cref="LoadException">Thrown when the version is not supported.</exception>
        public static void CheckVersion(DdpSection sec)
        {
            if(!IsSupportedVersion(sec)) { throw new LoadException("File has incompatible version."); }
        }
    }
}
