﻿using System;



namespace Robbiblubber.BadWolf.Struct.Exceptions
{
    /// <summary>This is the base exception class for the Bad Wolf structure library.</summary>
    public class GenericException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public GenericException() : base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        public GenericException(string message) : base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner exception.</param>
        public GenericException(string message, Exception innerException) : base(message, innerException)
        {}
    }
}
