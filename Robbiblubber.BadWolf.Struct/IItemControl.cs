﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Item controls implement this interface.</summary>
    public interface IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // events                                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        event ItemEventHandler ItemChanged;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        Control GetControl();


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        void ShowItem(IItem item, Project project);
    }
}
