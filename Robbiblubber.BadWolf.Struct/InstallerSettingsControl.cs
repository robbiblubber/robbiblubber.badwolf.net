﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.BadWolf.Struct;
using Robbiblubber.Util.Library;

namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a basic item view control.</summary>
    public partial class InstallerSettingsControl: UserControl, IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        private Installer _Item;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public InstallerSettingsControl()
        {
            InitializeComponent();

            _CboLevel.Items.Add("As Invoker");
            _CboLevel.Items.Add("Highest Available");
            _CboLevel.Items.Add("Require Administrator");
            _CboLevel.SelectedIndex = 0;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemControl                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        public event ItemEventHandler ItemChanged;


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        public void ShowItem(IItem item, Project project)
        {
            _Active = false;
            _Item = (Installer) item;
            _Project = project;

            _CboOutput.Text = _Item.Settings.OutputDirectory;
            _TextVersion.Text = _Item.Settings.CurrentVersion.ToVersionString();
            _TextMinimum.Text = _Item.Settings.MinimumVersion.ToVersionString();
            _CboLevel.SelectedIndex = (int) _Item.Settings.ExecutionLevel;
            _TextPublisher.Text = _Item.Settings.PublisherName;

            _Active = true;
            Visible = true;
        }


        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        public Control GetControl()
        {
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            if(_Item == null) return;
            bool idchanged = (_Item.ID != _TextVersion.Text);
            if(_Active)
            {
                _Item.Settings.OutputDirectory  = _CboOutput.Text;

                _Item.Settings.CurrentVersion = VersionOp.ToVersion(_TextVersion.Text);
                _Item.Settings.MinimumVersion = VersionOp.ToVersion(_TextMinimum.Text);
                _Item.Settings.ExecutionLevel = (ExecutionLevel) _CboLevel.SelectedIndex;
                _Item.Settings.PublisherName = _TextPublisher.Text;

                ItemChanged?.Invoke(this, new ItemEventArgs(_Item));
            }

            if(string.IsNullOrEmpty(_CboOutput.Text))
            {
                _ToolTip.SetToolTip(_LabelOutputError, "Output directory is empty");
                _LabelOutputError.Visible = true;
            }
            else if(File.Exists(_CboOutput.Text))
            {
                _ToolTip.SetToolTip(_LabelOutputError, "Output directory is invalid");
                _LabelOutputError.Visible = true;
            }
            else if(!FileOp.ValidFileName(_CboOutput.Text))
            {
                _ToolTip.SetToolTip(_LabelOutputError, "Output directory is invalid");
                _LabelOutputError.Visible = true;
            }
            else { _LabelOutputError.Visible = false; }

            if(!VersionOp.IsVersion(_TextVersion.Text))
            {
                _ToolTip.SetToolTip(_LabelVersionError, "Current version is invalid");
                _LabelVersionError.Visible = true;
            }
            else { _LabelVersionError.Visible = false; }

            if(!VersionOp.IsVersion(_TextMinimum.Text))
            {
                _ToolTip.SetToolTip(_LabelMinimumError, "Minimum version is invalid");
                _LabelMinimumError.Visible = true;
            }
            else if(_Item.Settings.MinimumVersion > _Item.Settings.CurrentVersion)
            {
                _ToolTip.SetToolTip(_LabelMinimumError, "Minimum version is higher than current");
                _LabelMinimumError.Visible = true;
            }
            else { _LabelMinimumError.Visible = false; }

            _LabelPublisherWarning.Visible = string.IsNullOrWhiteSpace(_TextPublisher.Text);
        }


        /// <summary>Item validating.</summary>
        private void _Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = (_LabelVersionError.Visible || _LabelOutputError.Visible);
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog d = new FolderBrowserDialog();

            string dir = _CboOutput.Text;
            while(!Directory.Exists(dir))
            {
                dir = Path.GetDirectoryName(dir);
                if(!dir.Contains(@"\")) break;
            }
            d.SelectedPath = dir;

            if(d.ShowDialog() == DialogResult.OK) { _CboOutput.Text = d.SelectedPath; }
        }
    }
}
