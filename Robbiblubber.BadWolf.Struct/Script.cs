﻿using System;
using System.IO;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a script.</summary>
    public class Script: Item, IItem
    {//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public Script(Project project, DdpSection sec = null): base(project, sec)
        {
            if(sec == null)
            {
                Body = File.ReadAllText(PathOp.ApplicationDirectory + @"\script.template").Replace("<$ScriptName>", "__" + ID);
            }
            else
            {
                Body = sec.GetString("body");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the script Body.</summary>
        public string Body
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "script");
            base.Save(parent);

            parent.SetString(ID + "/body", Body);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            LogLevel rval = LogLevel.TRACE;

            compiler.GiveFeedback(LogLevel.TRACE, "Creating script [" + ID + "] \"" + Name + "\".");
            File.WriteAllText(compiler.TargetDirectory + @"\" + ID + ".script", Body);

            return rval;
        }
    }
}
