﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class ScriptControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScriptControl));
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LabelBodyWarning = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelNameError = new System.Windows.Forms.Label();
            this._LabelBody = new System.Windows.Forms.Label();
            this._TextBody = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 77);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.Location = new System.Drawing.Point(30, 93);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            this._TextID.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(27, 355);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 3;
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(30, 371);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 113);
            this._TextDescription.TabIndex = 3;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            this._TextName.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelBodyWarning
            // 
            this._LabelBodyWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelBodyWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelBodyWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelBodyWarning.Image")));
            this._LabelBodyWarning.Location = new System.Drawing.Point(7, 155);
            this._LabelBodyWarning.Name = "_LabelBodyWarning";
            this._LabelBodyWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelBodyWarning.TabIndex = 2;
            this._ToolTip.SetToolTip(this._LabelBodyWarning, "Body is empty");
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(9, 93);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 1;
            this._LabelIdError.Visible = false;
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(9, 43);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._LabelNameError.Visible = false;
            // 
            // _LabelBody
            // 
            this._LabelBody.AutoSize = true;
            this._LabelBody.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelBody.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelBody.Location = new System.Drawing.Point(27, 139);
            this._LabelBody.Name = "_LabelBody";
            this._LabelBody.Size = new System.Drawing.Size(35, 13);
            this._LabelBody.TabIndex = 2;
            this._LabelBody.Text = "&Body:";
            // 
            // _TextBody
            // 
            this._TextBody.BackColor = System.Drawing.Color.MintCream;
            this._TextBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextBody.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextBody.Location = new System.Drawing.Point(30, 155);
            this._TextBody.Name = "_TextBody";
            this._TextBody.Size = new System.Drawing.Size(622, 181);
            this._TextBody.TabIndex = 2;
            this._TextBody.Text = "";
            this._TextBody.WordWrap = false;
            this._TextBody.TextChanged += new System.EventHandler(this._Changed);
            // 
            // ScriptControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._TextBody);
            this.Controls.Add(this._LabelBodyWarning);
            this.Controls.Add(this._LabelBody);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ScriptControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _LabelBodyWarning;
        private System.Windows.Forms.Label _LabelBody;
        private System.Windows.Forms.RichTextBox _TextBody;
    }
}
