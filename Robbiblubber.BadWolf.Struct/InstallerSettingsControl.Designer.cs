﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class InstallerSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstallerSettingsControl));
            this._LabelVersion = new System.Windows.Forms.Label();
            this._TextVersion = new System.Windows.Forms.TextBox();
            this._LabelOutput = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._LabelPublisherWarning = new System.Windows.Forms.Label();
            this._LabelOutputError = new System.Windows.Forms.Label();
            this._CboOutput = new System.Windows.Forms.ComboBox();
            this._LabelVersionError = new System.Windows.Forms.Label();
            this._TextPublisher = new System.Windows.Forms.TextBox();
            this._LabelPublisher = new System.Windows.Forms.Label();
            this._TextMinimum = new System.Windows.Forms.TextBox();
            this._LabelMinimumError = new System.Windows.Forms.Label();
            this._LabelMinimum = new System.Windows.Forms.Label();
            this._LabelLevel = new System.Windows.Forms.Label();
            this._CboLevel = new Robbiblubber.Util.Controls.RichComboBox();
            this.SuspendLayout();
            // 
            // _LabelVersion
            // 
            this._LabelVersion.AutoSize = true;
            this._LabelVersion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersion.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelVersion.Location = new System.Drawing.Point(27, 79);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(90, 13);
            this._LabelVersion.TabIndex = 2;
            this._LabelVersion.Text = "Current &Version:";
            // 
            // _TextVersion
            // 
            this._TextVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextVersion.Location = new System.Drawing.Point(30, 95);
            this._TextVersion.Name = "_TextVersion";
            this._TextVersion.Size = new System.Drawing.Size(304, 25);
            this._TextVersion.TabIndex = 2;
            this._TextVersion.TextChanged += new System.EventHandler(this._Changed);
            this._TextVersion.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelOutput
            // 
            this._LabelOutput.AutoSize = true;
            this._LabelOutput.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelOutput.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelOutput.Location = new System.Drawing.Point(27, 27);
            this._LabelOutput.Name = "_LabelOutput";
            this._LabelOutput.Size = new System.Drawing.Size(97, 13);
            this._LabelOutput.TabIndex = 0;
            this._LabelOutput.Text = "&Output Directory:";
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(627, 43);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 1;
            this._ButtonBrowse.TabStop = false;
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _LabelPublisherWarning
            // 
            this._LabelPublisherWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPublisherWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelPublisherWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelPublisherWarning.Image")));
            this._LabelPublisherWarning.Location = new System.Drawing.Point(9, 193);
            this._LabelPublisherWarning.Name = "_LabelPublisherWarning";
            this._LabelPublisherWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelPublisherWarning.TabIndex = 5;
            this._ToolTip.SetToolTip(this._LabelPublisherWarning, "Publisher name is empty");
            this._LabelPublisherWarning.Visible = false;
            // 
            // _LabelOutputError
            // 
            this._LabelOutputError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelOutputError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelOutputError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelOutputError.Image")));
            this._LabelOutputError.Location = new System.Drawing.Point(9, 43);
            this._LabelOutputError.Name = "_LabelOutputError";
            this._LabelOutputError.Size = new System.Drawing.Size(25, 25);
            this._LabelOutputError.TabIndex = 0;
            this._LabelOutputError.Visible = false;
            // 
            // _CboOutput
            // 
            this._CboOutput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this._CboOutput.FormattingEnabled = true;
            this._CboOutput.Location = new System.Drawing.Point(30, 43);
            this._CboOutput.Name = "_CboOutput";
            this._CboOutput.Size = new System.Drawing.Size(591, 25);
            this._CboOutput.TabIndex = 8;
            this._CboOutput.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelVersionError
            // 
            this._LabelVersionError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersionError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelVersionError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelVersionError.Image")));
            this._LabelVersionError.Location = new System.Drawing.Point(9, 95);
            this._LabelVersionError.Name = "_LabelVersionError";
            this._LabelVersionError.Size = new System.Drawing.Size(25, 25);
            this._LabelVersionError.TabIndex = 2;
            this._LabelVersionError.Visible = false;
            // 
            // _TextPublisher
            // 
            this._TextPublisher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPublisher.Location = new System.Drawing.Point(30, 193);
            this._TextPublisher.Name = "_TextPublisher";
            this._TextPublisher.Size = new System.Drawing.Size(622, 25);
            this._TextPublisher.TabIndex = 5;
            this._TextPublisher.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelPublisher
            // 
            this._LabelPublisher.AutoSize = true;
            this._LabelPublisher.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPublisher.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelPublisher.Location = new System.Drawing.Point(27, 177);
            this._LabelPublisher.Name = "_LabelPublisher";
            this._LabelPublisher.Size = new System.Drawing.Size(90, 13);
            this._LabelPublisher.TabIndex = 5;
            this._LabelPublisher.Text = "&Publisher Name:";
            // 
            // _TextMinimum
            // 
            this._TextMinimum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextMinimum.Location = new System.Drawing.Point(30, 139);
            this._TextMinimum.Name = "_TextMinimum";
            this._TextMinimum.Size = new System.Drawing.Size(304, 25);
            this._TextMinimum.TabIndex = 3;
            this._TextMinimum.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelMinimumError
            // 
            this._LabelMinimumError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelMinimumError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelMinimumError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelMinimumError.Image")));
            this._LabelMinimumError.Location = new System.Drawing.Point(9, 139);
            this._LabelMinimumError.Name = "_LabelMinimumError";
            this._LabelMinimumError.Size = new System.Drawing.Size(25, 25);
            this._LabelMinimumError.TabIndex = 3;
            this._LabelMinimumError.Visible = false;
            // 
            // _LabelMinimum
            // 
            this._LabelMinimum.AutoSize = true;
            this._LabelMinimum.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelMinimum.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelMinimum.Location = new System.Drawing.Point(27, 123);
            this._LabelMinimum.Name = "_LabelMinimum";
            this._LabelMinimum.Size = new System.Drawing.Size(99, 13);
            this._LabelMinimum.TabIndex = 3;
            this._LabelMinimum.Text = "&Minimum Version:";
            // 
            // _LabelLevel
            // 
            this._LabelLevel.AutoSize = true;
            this._LabelLevel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLevel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLevel.Location = new System.Drawing.Point(345, 79);
            this._LabelLevel.Name = "_LabelLevel";
            this._LabelLevel.Size = new System.Drawing.Size(88, 13);
            this._LabelLevel.TabIndex = 4;
            this._LabelLevel.Text = "Execution &Level:";
            // 
            // _CboLevel
            // 
            this._CboLevel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._CboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboLevel.FormattingEnabled = true;
            this._CboLevel.ImageList = null;
            this._CboLevel.Location = new System.Drawing.Point(348, 95);
            this._CboLevel.Name = "_CboLevel";
            this._CboLevel.Size = new System.Drawing.Size(304, 26);
            this._CboLevel.TabIndex = 4;
            this._CboLevel.SelectedIndexChanged += new System.EventHandler(this._Changed);
            // 
            // InstallerSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._CboLevel);
            this.Controls.Add(this._LabelLevel);
            this.Controls.Add(this._TextMinimum);
            this.Controls.Add(this._LabelMinimumError);
            this.Controls.Add(this._LabelMinimum);
            this.Controls.Add(this._TextPublisher);
            this.Controls.Add(this._LabelPublisherWarning);
            this.Controls.Add(this._LabelPublisher);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._CboOutput);
            this.Controls.Add(this._TextVersion);
            this.Controls.Add(this._LabelVersionError);
            this.Controls.Add(this._LabelOutputError);
            this.Controls.Add(this._LabelVersion);
            this.Controls.Add(this._LabelOutput);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "InstallerSettingsControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.TextBox _TextVersion;
        private System.Windows.Forms.Label _LabelOutput;
        private System.Windows.Forms.Label _LabelOutputError;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.ComboBox _CboOutput;
        private System.Windows.Forms.Label _LabelVersionError;
        private System.Windows.Forms.TextBox _TextPublisher;
        private System.Windows.Forms.Label _LabelPublisherWarning;
        private System.Windows.Forms.Label _LabelPublisher;
        private System.Windows.Forms.TextBox _TextMinimum;
        private System.Windows.Forms.Label _LabelMinimumError;
        private System.Windows.Forms.Label _LabelMinimum;
        private System.Windows.Forms.Label _LabelLevel;
        private Util.Controls.RichComboBox _CboLevel;
    }
}
