﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Installer child elements implement this interface.</summary>
    public interface IInstallerChild: IShowable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent installer.</summary>
        Installer Installer { get; }
    }
}
