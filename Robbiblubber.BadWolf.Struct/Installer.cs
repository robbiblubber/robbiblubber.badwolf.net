﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.BadWolf.Struct.Exceptions;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a Bad Wolf Installer.</summary>
    public class Installer: Item, IItem, IContainerItem, ILocalizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saved project source.</summary>
        private string _SavedSource;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="fileName">File name.</param>
        public Installer(Project project, string fileName): base(project, null)
        {
            Settings = new InstallerSettings(this);
            Steps = new InstallerStepsFolder(this);
            Packages = new InstallerPackagesFolder(this);

            Ddp ddp = Ddp.Load(fileName);
            DdpSection sec = null;

            if(ddp.GetString("file") != "bwnst") throw new LoadException("File is invalid.");
            CommonStandards.CheckVersion(ddp);

            foreach(DdpSection i in ddp.Sections)
            {
                if(i.GetString("type") == "installer") { sec = i; break; }
            }

            if(sec == null) throw new LoadException("File is invalid.");

            FileName = fileName;
            ID = sec.Name;
            Name = sec.GetString("name");
            Description = sec.GetString("description");

            NameLocaleKey = sec.GetString("lk.name");
            DescriptionLocaleKey = sec.GetString("lk.description");

            Settings.Load(sec);
            Packages.Load(sec);

            foreach(DdpSection i in sec.Sections)
            {
                IItem item = i.LoadItem(Project);
                if(item != null) { Items.Add(item); }
            }

            _SavedSource = _GetDdp().Text;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="fileName">File name.</param>
        /// <param name="name">Project name.</param>
        /// <param name="description">Project description.</param>
        public Installer(Project project, string fileName, string id, string name, string description): base(project, null)
        {
            Settings = new InstallerSettings(this);
            Steps = new InstallerStepsFolder(this);
            Packages = new InstallerPackagesFolder(this);

            FileName = fileName;
            ID = id;
            Name = name;
            Description = description;

            FileName = fileName;
            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the project ddp.</summary>
        /// <returns>ddp.</returns>
        private Ddp _GetDdp()
        {
            Ddp rval = new Ddp();
            rval.SetString("file", "bwnst");
            rval.SetString("version", VersionOp.ApplicationVersion.ToVersionString());

            Save(rval);
            Settings.Save(rval);
            Packages.Save(rval);

            DdpSection p = rval.Sections[ID];
            foreach(IItem i in Items) { i.Save(p); }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the project file name.</summary>
        public string FileName { get; }


        /// <summary>Gets the installer settings.</summary>
        public InstallerSettings Settings { get; protected set; }


        /// <summary>Gets the steps in this installer.</summary>
        public InstallerStepsFolder Steps
        {
            get; private set;
        }


        /// <summary>Gets the packages in this installer.</summary>
        public InstallerPackagesFolder Packages
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Saves the item.</summary>
        public void Save()
        {
            _SavedSource = _GetDdp().Text;
            File.WriteAllText(FileName, _SavedSource);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IContainerItem                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items in this installer.</summary>
        public IMutableList<IItem> Items 
        { 
            get; 
        } = new ItemList<IItem>();


        /// <summary>Returns the item with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Item.</returns>
        public IItem GetItem(string id)
        {
            if(ID == id) { return this; }

            foreach(IItem i in Items)
            {
                if(i.ID == id) return i;
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILocalizable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the name locale key.</summary>
        public string NameLocaleKey
        {
            get; set;
        }


        /// <summary>Gets or sets the description locale key.</summary>
        public string DescriptionLocaleKey
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "installer");
            base.Save(parent);

            parent.SetString(ID + "/lk.name", NameLocaleKey);
            parent.SetString(ID + "/lk.description", DescriptionLocaleKey);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            // TODO: implement compile method.
            return LogLevel.ERROR;
        }
    }
}
