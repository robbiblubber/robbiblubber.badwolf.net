﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class allows access to controllers.</summary>
    public static class ControllerManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Controllers.</summary>
        private static Dictionary<Type, IItemController> _InternalControllers = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the controller dictionary.</summary>
        public static Dictionary<Type, IItemController> Controllers
        {
            get
            {
                if(_InternalControllers == null)
                {
                    _InternalControllers = new Dictionary<Type, IItemController>();
                    foreach(IItemController i in ClassOp.LoadSubtypesOf<IItemController>())
                    {
                        _InternalControllers.Add(i.ItemType, i);
                    }
                }

                return _InternalControllers;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the controller for an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Controller.</returns>
        public static IItemController GetController(this IItem item)
        {
            if(Controllers.ContainsKey(item.GetType())) { return Controllers[item.GetType()]; }
            return null;
        }


        /// <summary>Loads an item from a ddp section.</summary>
        /// <param name="sec">ddp Section.</param>
        /// <param name="project">Project.</param>
        /// <returns>Item.</returns>
        public static IItem LoadItem(this DdpSection sec, Project project)
        {
            foreach(IItemController i in Controllers.Values)
            {
                if(i.TypeKey == sec.GetString("type"))
                {
                    return (IItem) Activator.CreateInstance(i.ItemType, project, sec);
                }
            }

            return null;
        }
    }
}
