﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides event arguments for feedback events.</summary>
    public class FeedbackEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="level">Log level.</param>
        /// <param name="message">Event message.</param>
        /// <param name="data">Event data.</param>
        public FeedbackEventArgs(LogLevel level, string message, string data = null)
        {
            Level = level;
            Message = message;
            Data = data;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the event log level.</summary>
        public LogLevel Level
        {
            get; protected set;
        }


        /// <summary>Gets the event log message.</summary>
        public string Message
        {
            get; protected set;
        }


        /// <summary>Gets the event log data.</summary>
        public string Data
        {
            get; protected set;
        }
    }
}
