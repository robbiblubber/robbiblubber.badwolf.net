﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a prerequisite folder.</summary>
    public class PrerequisiteFolder: Folder, IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public PrerequisiteFolder(Project project, DdpSection sec = null): base(project, sec)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Folder                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the folder.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "folder:prerequisite");
            base.Save(parent);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            compiler.GiveFeedback(LogLevel.TRACE, "Compiling prerequisite folder [" + ID + "] \"" + Name + "\".");
            LogLevel rval = _Copy(compiler);

            compiler.GiveFeedback(LogLevel.TRACE, "Creating prerequisite folder configuration [" + ID + "] \"" + Name + "\".");
            Ddp cfg = new Ddp();

            cfg.SetString("id", ID);
            cfg.SetString("type", "folder:prerequisite");
            cfg.SetString("name", Name);
            cfg.Save(compiler.TargetDirectory + @"\" + ID + ".config");

            return rval;
        }
    }
}
