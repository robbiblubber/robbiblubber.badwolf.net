﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a configuration file.</summary>
    public class ConfigurationFile: Item, IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public ConfigurationFile(Project project, DdpSection sec = null): base(project, sec)
        {
            if(sec != null)
            {
                Target = sec.GetString("target");
                Values = sec.GetString("values");
                Clean  = sec.GetBoolean("clean");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the target configuration file.</summary>
        public string Target
        {
            get; set;
        }


        /// <summary>Gets or sets the configuration file values.</summary>
        public string Values
        {
            get; set;
        }


        /// <summary>Gets or sets if existing configuration file values will be removed.</summary>
        public bool Clean
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "configfile");
            base.Save(parent);

            parent.SetString(ID + "/target", Target);
            parent.SetString(ID + "/values", Values);
            parent.SetBoolean(ID + "/clean", Clean);
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            LogLevel rval = LogLevel.TRACE;

            if(string.IsNullOrWhiteSpace(Values))
            {
                compiler.GiveFeedback(LogLevel.WARNING, "[" + ID + "] \"" + Name + "\" has no values.");
                rval = LogLevel.WARNING;
            }

            compiler.GiveFeedback(LogLevel.TRACE, "Creating configuration file configuration [" + ID + "] \"" + Name + "\".");
            Ddp cfg = new Ddp();
            Save(cfg);
            cfg.Save(compiler.TargetDirectory + @"\" + ID + ".config");

            return rval;
        }
    }
}
