﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.BadWolf.Struct.Exceptions;
using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements a Bad Wolf project.</summary>
    public class Project: Item, IItem, ILocalizable, IImmovable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected constants                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Allowed characters for IDs.</summary>
        protected const string _ID_CHARS = "+-_.:~#&*()[]!?<>0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saved project source.</summary>
        private string _SavedSource;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Project(): base(null, null)
        {
            Project = this;
            Installers = new ItemList<Installer>();
            Packages = new ItemList<Package>();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        public Project(string fileName): this()
        {
            Ddp ddp = Ddp.Load(fileName);
            
            DdpSection sec = null;

            if(ddp.GetString("file") != "bwprj") throw new LoadException("File is invalid.");
            CommonStandards.CheckVersion(ddp);

            foreach(DdpSection i in ddp.Sections)
            {
                if(i.GetString("type") == "project") { sec = i; break; }
            }

            if(sec == null) throw new LoadException("File is invalid.");

            FileName = fileName;
            ID = sec.GetString("id");
            Name = sec.GetString("name");
            Description = sec.GetString("description");

            NameLocaleKey = sec.GetString("lk.name");
            DescriptionLocaleKey = sec.GetString("lk.description");

            foreach(DdpEntry i in sec.Sections["installers"].Entries)
            {
                Installer s = new Installer(this, i.StringValue.Contains(':') ? i.StringValue : (Path.GetDirectoryName(fileName) + '\\' + i.StringValue));
                Installers.Add(s);
            }

            foreach(DdpEntry i in sec.Sections["packages"].Entries)
            {
                Package s = new Package(this, i.StringValue.Contains(':') ? i.StringValue : (Path.GetDirectoryName(fileName) + '\\' + i.StringValue));
                Packages.Add(s);
            }

            _SavedSource = _GetDdp().Text;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="id">Project ID.</param>
        /// <param name="name">Project name.</param>
        /// <param name="description">Project description.</param>
        public Project(string fileName, string id, string name, string description): this()
        {
            FileName = fileName;
            ID = id;
            Name = name;
            Description = description;

            FileName = fileName;
            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an installer package in a subtree.</summary>
        /// <param name="start">Subtree starting point.</param>
        /// <param name="id">Package ID.</param>
        /// <returns>Returns the first instaler package with this ID, if none exists returns NULL.</returns>
        private static InstallerPackage _GetInstallerPackage(IInstallerPackageParent start, string id)
        {
            if((start is InstallerPackage) && (((InstallerPackage) start).ID == id)) { return (InstallerPackage) start; }

            InstallerPackage rval;
            foreach(InstallerPackage i in start.Packages)
            {
                if((rval = _GetInstallerPackage(i, id)) != null) { return rval; }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the project ddp.</summary>
        /// <returns>ddp.</returns>
        private Ddp _GetDdp()
        {
            Ddp rval = new Ddp();
            rval.SetString("file", "bwprj");
            rval.SetString("version", VersionOp.ApplicationVersion.ToVersionString());

            DdpSection p = rval.Sections.Add(ID);
            p.SetString("type", "project");
            p.SetString("id", ID);
            p.SetString("name", Name);
            p.SetString("decription", Description);

            p.SetString("lk.name", NameLocaleKey);
            p.SetString("lk.description", DescriptionLocaleKey);

            int n = 0;
            foreach(Installer i in Installers)
            {
                p.SetString("installers/[" + (n++).ToString() + "]", FileOp.GetRelativePath(Path.GetDirectoryName(FileName), i.FileName));
            }

            n = 0;
            foreach(Package i in Packages)
            {
                p.SetString("packages/[" + (n++).ToString() + "]", FileOp.GetRelativePath(Path.GetDirectoryName(FileName), i.FileName));
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if an item ID valid.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        /// <returns>Returns 0 if the ID is valid, -1 for empty ID, -2 for malformed, and -3 for ambiguous ID.</returns>
        public static int CheckID(string id, Project project)
        {
            if(project != null) { return project.CheckID(id); }

            if(string.IsNullOrWhiteSpace(id)) { return -1; }

            foreach(char i in id)
            {
                if(!_ID_CHARS.Contains(i)) { return -2; }
            }
            return 0;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the project file name.</summary>
        public string FileName 
        { 
            get; 
        }


        /// <summary>Gets the installers in this project.</summary>
        public ItemList<Installer> Installers
        {
            get; protected set;
        }


        /// <summary>Gets the packages in this project.</summary>
        public ItemList<Package> Packages
        {
            get; protected set;
        }


        /// <summary>Gets if the project has changes.</summary>
        public bool HasChanges
        {
            get
            {
                // TODO: check packages and installer!

                return (_GetDdp().Text != _SavedSource);
            }
        }


        /// <summary>Gets all items in this project including the project item itself.</summary>
        public IEnumerable<IItem> Items
        {
            get
            {
                List<IItem> rval = new List<IItem>();
                rval.Add(this);

                foreach(Installer i in Installers)
                {
                    rval.Add(i);
                    rval.AddRange(i.Items);
                }

                foreach(Package i in Packages)
                {
                    rval.Add(i);
                    rval.AddRange(i.Items);
                }

                return rval;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a package by its ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Package.</returns>
        public Package GetPackage(string id)
        {
            foreach(Package i in Packages)
            {
                if(i.ID == id) { return i; }
            }

            return GetInstallerPackage(id)?.Package;
        }


        /// <summary>Gets an installer package in a subtree.</summary>
        /// <param name="start">Subtree starting point.</param>
        /// <param name="id">Package ID.</param>
        /// <returns>Returns the first instaler package with this ID, if none exists returns NULL.</returns>
        public InstallerPackage GetInstallerPackage(string id)
        {
            InstallerPackage rval;
            foreach(Installer i in Installers)
            {
                if((rval = _GetInstallerPackage(i.Packages, id)) != null) { return rval; }
            }

            return null;
        }


        /// <summary>Returns if an item ID valid.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns 0 if the ID is valid, -1 for empty ID, -2 for malformed, and -3 for ambiguous ID.</returns>
        public int CheckID(IItem item)
        {
            foreach(IItem i in Items)
            {
                if((i.ID == item.ID) && (i != item)) return -3;
            }

            return CheckID(item.ID, null);
        }


        /// <summary>Returns if an item ID valid.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns 0 if the ID is valid, -1 for empty ID, -2 for malformed, and -3 for ambiguous ID.</returns>
        public int CheckID(string id)
        {
            foreach(IItem i in Items)
            {
                if(i.ID == id) return -3;
            }

            return CheckID(id, null);
        }


        /// <summary>Returns if an item can move.</summary>
        /// <param name="item">Item.</param>
        /// <param name="direction">Direction.</param>
        /// <returns>Returns TRUE if the item can be moved up in that direction, otherwise returns FALSE.</returns>
        public bool CanMove(IShowable item, int direction)
        {
            if(item is IImmovable)  { return false; }

            if(direction >= 0)
            {
                return (GetSiblings(item).Max(m => m.SortIndex) > item.GetItem().SortIndex);
            }
            
            return (GetSiblings(item).Min(m => m.SortIndex) < item.GetItem().SortIndex);
        }


        /// <summary>Returns if an item can move up.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the item can be moved up, otherwise returns FALSE.</returns>
        public bool CanMoveUp(IShowable item)
        {
            return CanMove(item, -1);
        }


        /// <summary>Returns if an item can move up.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the item can be moved up, otherwise returns FALSE.</returns>
        public bool CanMoveDown(IShowable item)
        {
            return CanMove(item, 1);
        }


        /// <summary>Moves an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="direction">Direction.</param>
        public void Move(IItem item, int direction)
        {
            if(item is Project) { return; }

            try
            {
                IItem last = null;
                foreach(IItem i in ((direction >= 0) ? GetSiblings(item).OrderByDescending(m => m.SortIndex) : GetSiblings(item).OrderBy(m => m.SortIndex)))
                {
                    if(i == item)
                    {
                        int t = last.SortIndex;
                        last.SortIndex = item.SortIndex;
                        item.SortIndex = t;

                        return;
                    }
                    last = i;
                }
            } catch(Exception) {}
        }


        /// <summary>Moves an item up.</summary>
        /// <param name="item">Item.</param>
        public void MoveUp(IItem item)
        {
            Move(item, -1);
        }


        /// <summary>Moves an item down.</summary>
        /// <param name="item">Item.</param>
        public void MoveDown(IItem item)
        {
            Move(item, 1);
        }


        /// <summary>Gets the parent item for an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Parent item.</returns>
        public IContainerItem GetParent(IItem item)
        {
            foreach(Installer i in Installers)
            {
                if(i.Items.Contains(item)) { return i; }
            }

            foreach(Package i in Packages)
            {
                if(i.Items.Contains(item)) { return i; }
            }

            return null;
        }


        /// <summary>Gets the item siblings.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Siblings.</returns>
        public IEnumerable<IItem> GetSiblings(IShowable item)
        {
            if(item is Installer) { return Installers; }
            if(item is Package)   { return Packages; }
            
            if(item is InstallerPackage) { return ((InstallerPackage) item).Siblings; }

            return GetParent(item.GetItem()).Items;
        }


        /// <summary>Saves the item.</summary>
        public void Save()
        {
            _SavedSource = _GetDdp().Text;
            File.WriteAllText(FileName, _SavedSource);

            foreach(Installer i in Installers) { i.Save(); }
            foreach(Package i in Packages) { i.Save(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IContainerItem                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the item with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Item.</returns>
        public IItem GetItem(string id)
        {
            if(ID == id) { return this; }

            IItem rval;
            foreach(IContainerItem i in Packages)
            {
                if((rval = i.GetItem(id)) != null) { return rval; }
            }

            foreach(IContainerItem i in Installers)
            {
                if((rval = i.GetItem(id)) != null) { return rval; }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILocalizable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the name locale key.</summary>
        public string NameLocaleKey 
        { 
            get; set; 
        }


        /// <summary>Gets or sets the description locale key.</summary>
        public string DescriptionLocaleKey 
        { 
            get; set; 
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            // TODO: implement compile method.
            return LogLevel.ERROR;
        }
    }
}
