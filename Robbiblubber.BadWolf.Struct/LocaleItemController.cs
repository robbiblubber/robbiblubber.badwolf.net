﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements a controller for locale items.</summary>
    public class LocaleItemController: IItemController
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemController                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item type.</summary>
        public Type ItemType
        {
            get { return typeof(LocaleItem); }
        }


        /// <summary>Gets the control type.</summary>
        public Type ControlType
        {
            get { return typeof(FolderControl); }
        }


        /// <summary>Gets the type key.</summary>
        public string TypeKey
        {
            get { return "locale"; }
        }


        /// <summary>Gets if the item is attachable on packages.</summary>
        public bool OnPackage
        {
            get { return true; }
        }


        /// <summary>Gets if the item is attachable on installer.</summary>
        public bool OnInstaller
        {
            get { return true; }
        }


        /// <summary>Gets if the item is removable.</summary>
        public bool Removable
        {
            get { return true; }
        }


        /// <summary>Gets the item name.</summary>
        public string ItemName
        {
            get { return "&Locale"; }
        }


        /// <summary>Gets the item image.</summary>
        public Image ItemImage
        {
            get { return Resources.locale; }
        }


        /// <summary>Gets the item image key.</summary>
        public string ItemImageKey
        {
            get { return "locale"; }
        }


        /// <summary>Gets the item keys.</summary>
        public Keys ItemKeys
        {
            get { return Keys.None; }
        }


        /// <summary>Gets the item sort index.</summary>
        public int SortIndex
        {
            get { return 220; }
        }
    }
}
