﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents the installer steps folder.</summary>
    public class InstallerStepsFolder: ItemList<IStep>, IInstallerChild, IShowable, IImmovable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Parent installer.</param>
        public InstallerStepsFolder(Installer installer): base()
        {
            Installer = installer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IShowable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent project.</summary>
        public Project Project 
        { 
            get { return Installer.Project; }
        }


        /// <summary>Gets the showable item.</summary>
        public IItem GetItem()
        {
            return Installer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInstallerChild                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent installer.</summary>
        public Installer Installer
        {
            get; private set;
        }
    }
}
