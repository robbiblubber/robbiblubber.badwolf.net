﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class represents a variable list.</summary>
    public class VariableList: Item, IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="project">Project.</param>
        /// <param name="sec">ddp Section.</param>
        public VariableList(Project project, DdpSection sec = null): base(project, sec)
        {
            if(sec != null)
            {
                foreach(DdpEntry i in sec.Sections["vars"].Entries)
                {
                    Variables.Add(i.Name, i.StringValue);
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the variables.</summary>
        public Dictionary<string, string> Variables
        {
            get; set;
        } = new Dictionary<string, string>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Item                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public override void Save(DdpSection parent)
        {
            parent.SetString(ID + "/type", "variables");
            base.Save(parent);

            foreach(KeyValuePair<string, string> i in Variables)
            {
                parent.SetString(ID + "/vars/" + i.Key, i.Value);
            }
        }


        /// <summary>Compiles the item.</summary>
        /// <param name="compiler">Compiler.</param>
        /// <returns>Returns the log level of the most critical event in the compilation process.</returns>
        public override LogLevel Compile(ICompiler compiler)
        {
            LogLevel rval = LogLevel.TRACE;

            compiler.GiveFeedback(LogLevel.TRACE, "Creating variable configuration [" + ID + "] \"" + Name + "\".");
            Ddp cfg = new Ddp();
            Save(cfg);
            cfg.Save(compiler.TargetDirectory + @"\" + ID + ".config");

            return rval;
        }
    }
}
