﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides event arguments for item events.</summary>
    public class ItemEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="item">Item.</param>
        public ItemEventArgs(IItem item)
        {
            Item = item;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the affected item.</summary>
        public IItem Item
        {
            get; protected set;
        }
    }
}
