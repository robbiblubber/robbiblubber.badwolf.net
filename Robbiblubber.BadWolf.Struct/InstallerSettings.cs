﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides installer settings.</summary>
    public class InstallerSettings: IInstallerChild, IShowable, IImmovable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Parent installer.</param>
        public InstallerSettings(Installer installer): base()
        {
            Installer = installer;

            OutputDirectory = PathOp.ApplicationDirectory + @"\out\" + Installer.ID;
            CurrentVersion = new Version(1, 0, 0);
            MinimumVersion = new Version(1, 0, 0);
            ExecutionLevel = ExecutionLevel.REQUIRE_ADMINISTRATOR;
            PublisherName = "";
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the output directory.</summary>
        public string OutputDirectory
        {
            get; set;
        }


        /// <summary>Gets or sets the current version.</summary>
        public Version CurrentVersion
        {
            get; set;
        }


        /// <summary>Gets or sets the minimum version.</summary>
        public Version MinimumVersion
        {
            get; set;
        }


        /// <summary>Gets or sets the installer execution level.</summary>
        public ExecutionLevel ExecutionLevel
        {
            get; set;
        }


        /// <summary>Gets or sets the publisher name.</summary>
        public string PublisherName
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Loads installer settings data.</summary>
        /// <param name="sec">ddp Section.</param>
        public void Load(DdpSection sec)
        {
            OutputDirectory = sec.GetString("settings/out");
            CurrentVersion = VersionOp.ToVersion(sec.GetString("settings/version"));
            MinimumVersion = VersionOp.ToVersion(sec.GetString("settings/minimum"));
            ExecutionLevel = (ExecutionLevel) sec.GetInteger("settings/level");
            PublisherName = sec.GetString("settings/publisher");
        }


        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public void Save(DdpSection parent)
        {
            parent.SetString(Installer.ID + "/settings/out", OutputDirectory);
            parent.SetString(Installer.ID + "/settings/version", CurrentVersion.ToVersionString());
            parent.SetString(Installer.ID + "/settings/minimum", MinimumVersion.ToVersionString());
            parent.SetInteger(Installer.ID + "/settings/level", (int) ExecutionLevel);
            parent.SetString(Installer.ID + "/settings/publisher", PublisherName);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IShowable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent project.</summary>
        public Project Project 
        { 
            get { return Installer.Project; }
        }


        /// <summary>Gets the showable item.</summary>
        public IItem GetItem()
        {
            return Installer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInstallerChild                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent installer.</summary>
        public Installer Installer
        {
            get; private set;
        }
    }
}
