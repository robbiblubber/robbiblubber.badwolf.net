﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a base implementation for installer package parents.</summary>
    public abstract class InstallerPackageParent: IInstallerPackageParent, IInstallerChild, IShowable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="installer">Installer.</param>
        protected InstallerPackageParent(Installer installer)
        {
            Installer = installer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads installer settings data.</summary>
        /// <param name="sec">ddp Section.</param>
        public virtual void Load(DdpSection sec)
        {
            foreach(DdpSection i in sec.Sections)
            {
                Packages.Add(new InstallerPackage(Installer, i));
            }
        }


        /// <summary>Saves the item.</summary>
        /// <param name="parent">Parent ddp section.</param>
        public virtual void Save(DdpSection parent)
        {
            foreach(InstallerPackage i in Packages) { i.Save(parent); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IShowable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent project.</summary>
        public Project Project 
        { 
            get { return Installer.Project; }
        }


        /// <summary>Gets the showable item.</summary>
        public virtual IItem GetItem()
        {
            return Installer;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInstallerChild                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent installer.</summary>
        public Installer Installer
        {
            get; protected set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInstallerPackageParent                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the child packages of this item.</summary>
        public IMutableList<InstallerPackage> Packages 
        { 
            get; protected set;
        } = new MutableList<InstallerPackage>();


        /// <summary>Gets all child packages of this item.</summary>
        public IImmutableList<Package> AllChildPackages 
        { 
            get { return _GetChildren(new TList<Package>(), this); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Recursively gets all child packages of an installer package parent.</summary>
        /// <param name="result">Result list.</param>
        /// <param name="parent">Installer package parent.</param>
        /// <returns>Child list.</returns>
        protected static IImmutableList<Package> _GetChildren(IMutableList<Package> result, IInstallerPackageParent parent)
        {
            foreach(InstallerPackage i in parent.Packages)
            {
                result.Add(i.Package);
                _GetChildren(result, i);
            }

            return result;
        }
    }
}
