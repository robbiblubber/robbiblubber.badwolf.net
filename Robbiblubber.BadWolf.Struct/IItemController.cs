﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Item controllers implement this interface.</summary>
    public interface IItemController
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the item type.</summary>
        Type ItemType { get; }


        /// <summary>Gets the control type.</summary>
        Type ControlType { get; }


        /// <summary>Gets the type key.</summary>
        string TypeKey { get; }


        /// <summary>Gets if the item is attachable on packages.</summary>
        bool OnPackage { get; }


        /// <summary>Gets if the item is attachable on installer.</summary>
        bool OnInstaller { get; }


        /// <summary>Gets if the item is removable.</summary>
        bool Removable { get; }


        /// <summary>Gets the item name.</summary>
        string ItemName { get; }


        /// <summary>Gets the item image.</summary>
        Image ItemImage { get; }


        /// <summary>Gets the item image key.</summary>
        string ItemImageKey { get; }


        /// <summary>Gets the item keys.</summary>
        Keys ItemKeys { get; }


        /// <summary>Gets the item sort index.</summary>
        int SortIndex { get; }
    }
}
