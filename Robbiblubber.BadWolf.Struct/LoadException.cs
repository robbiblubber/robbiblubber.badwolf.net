﻿using System;



namespace Robbiblubber.BadWolf.Struct.Exceptions
{
    /// <summary>This is thrown when an item could not be loaded.</summary>
    public class LoadException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public LoadException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        public LoadException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Inner exception.</param>
        public LoadException(string message, Exception innerException) : base(message, innerException)
        {}
    }
}
