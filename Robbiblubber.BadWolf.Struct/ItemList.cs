﻿using System;
using System.Linq;

using Robbiblubber.Util.Library.Collections;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides an item list.</summary>
    public class ItemList<T>: MutableList<T>, IMutableList<T> where T: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an item by its ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Item.</returns>
        public T this[string id]
        {
            get
            {
                foreach(T i in this)
                {
                    if(i.ID == id) return i;
                }

                return default;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Mutable<IItem>                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public override void Add(T item)
        {
            if(Count == 0)
            {
                item.SortIndex = 2400;
            }
            else { item.SortIndex = (this.Max(m => m.SortIndex) + 2); }

            base.Add(item);
        }
    }
}
