﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class implements a controller for variable lists.</summary>
    public class VariableListController: IItemController
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemController                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item type.</summary>
        public Type ItemType
        {
            get { return typeof(VariableList); }
        }


        /// <summary>Gets the control type.</summary>
        public Type ControlType
        {
            get { return typeof(VariableControl); }
        }


        /// <summary>Gets the type key.</summary>
        public string TypeKey
        {
            get { return "variables"; }
        }


        /// <summary>Gets if the item is attachable on packages.</summary>
        public bool OnPackage
        {
            get { return true; }
        }


        /// <summary>Gets if the item is attachable on installer.</summary>
        public bool OnInstaller
        {
            get { return true; }
        }


        /// <summary>Gets if the item is removable.</summary>
        public bool Removable
        {
            get { return true; }
        }


        /// <summary>Gets the item name.</summary>
        public string ItemName
        {
            get { return "&Variables"; }
        }


        /// <summary>Gets the item image.</summary>
        public Image ItemImage
        {
            get { return Resources.variables; }
        }


        /// <summary>Gets the item image key.</summary>
        public string ItemImageKey
        {
            get { return "variables"; }
        }


        /// <summary>Gets the item keys.</summary>
        public Keys ItemKeys
        {
            get { return Keys.None; }
        }


        /// <summary>Gets the item sort index.</summary>
        public int SortIndex
        {
            get { return 205; }
        }
    }
}
