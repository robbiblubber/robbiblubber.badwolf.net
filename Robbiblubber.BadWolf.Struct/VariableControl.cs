﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a variable control.</summary>
    public partial class VariableControl: UserControl, IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        private VariableList _Item;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;

        /// <summary>Variable data table.</summary>
        private DataTable _Vars;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public VariableControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemControl                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        public event ItemEventHandler ItemChanged;


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        public void ShowItem(IItem item, Project project)
        {
            _Active = false;
            _Item = (VariableList) item;
            _Project = project;

            _TextName.Text = item.Name;
            _TextID.Text = item.ID;
            _TextCondition.Text = item.Condition;
            _TextDescription.Text = item.Description;

            _Vars = new DataTable();
            _Vars.Columns.Add("Variable", typeof(string));
            _Vars.Columns.Add("Value", typeof(string));
            foreach(KeyValuePair<string, string> i in _Item.Variables) { _Vars.Rows.Add(i.Key, i.Value); }
            _GridVariables.DataSource = _Vars;

            _Active = true;
            Visible = true;
        }


        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        public Control GetControl()
        {
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item changed.</summary>
        private void _Change(object sender, DataGridViewCellCancelEventArgs e)
        {
            _Changed(sender, (EventArgs) e);
        }


        /// <summary>Item changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            bool idchanged = (_Item.ID != _TextID.Text);
            if(_Active)
            {
                _Item.Name = _TextName.Text;
                _Item.ID = _TextID.Text;
                _Item.Description = _TextDescription.Text;

                List<string> dep = new List<string>();
                

                ItemChanged?.Invoke(this, new ItemEventArgs(_Item));
            }

            _LabelConditionHint.Visible = (!string.IsNullOrWhiteSpace(_TextCondition.Text));
            _LabelNameError.Visible = string.IsNullOrEmpty(_TextName.Text);

            if(sender == _GridVariables)
            {
                _Item.Variables.Clear();
                foreach(DataRow i in _Vars.Rows)
                {
                    if(!string.IsNullOrWhiteSpace((string) i[0])) { _Item.Variables.Add((string) i[0], (string) i[1]); }
                }
            }


            switch(_Project.CheckID(_Item))
            {
                case -1:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is empty");
                    _LabelIdError.Visible = true;
                    break;
                case -2:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is invalid");
                    _LabelIdError.Visible = true;
                    break;
                case -3:
                    _ToolTip.SetToolTip(_LabelIdError, "ID is ambiguous");
                    _LabelIdError.Visible = true;
                    break;
                default:
                    _LabelIdError.Visible = false; break;
            }
        }


        /// <summary>Item validating.</summary>
        private void _Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = (_LabelIdError.Visible || _LabelNameError.Visible);
        }
    }
}
