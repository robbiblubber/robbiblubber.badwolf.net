﻿namespace Robbiblubber.BadWolf.Struct
{
    partial class ConfigurationFileControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationFileControl));
            this._LabelID = new System.Windows.Forms.Label();
            this._TextID = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelTarget = new System.Windows.Forms.Label();
            this._TextTarget = new System.Windows.Forms.TextBox();
            this._LabelTargetWarning = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LabelNameError = new System.Windows.Forms.Label();
            this._LabelIdError = new System.Windows.Forms.Label();
            this._LabelValuesWarning = new System.Windows.Forms.Label();
            this._TextValues = new System.Windows.Forms.TextBox();
            this._LabelValues = new System.Windows.Forms.Label();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._CheckClean = new System.Windows.Forms.CheckBox();
            this._TextCondition = new System.Windows.Forms.TextBox();
            this._LabelConditionHint = new System.Windows.Forms.Label();
            this._LabelCondition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _LabelID
            // 
            this._LabelID.AutoSize = true;
            this._LabelID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelID.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelID.Location = new System.Drawing.Point(27, 73);
            this._LabelID.Name = "_LabelID";
            this._LabelID.Size = new System.Drawing.Size(21, 13);
            this._LabelID.TabIndex = 1;
            this._LabelID.Text = "&ID:";
            // 
            // _TextID
            // 
            this._TextID.BackColor = System.Drawing.SystemColors.Window;
            this._TextID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextID.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextID.Location = new System.Drawing.Point(30, 89);
            this._TextID.Name = "_TextID";
            this._TextID.Size = new System.Drawing.Size(622, 25);
            this._TextID.TabIndex = 1;
            this._TextID.TextChanged += new System.EventHandler(this._Changed);
            this._TextID.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(27, 27);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BackColor = System.Drawing.SystemColors.Window;
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextName.Location = new System.Drawing.Point(30, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(622, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            this._TextName.Validating += new System.ComponentModel.CancelEventHandler(this._Validating);
            // 
            // _LabelTarget
            // 
            this._LabelTarget.AutoSize = true;
            this._LabelTarget.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTarget.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTarget.Location = new System.Drawing.Point(27, 126);
            this._LabelTarget.Name = "_LabelTarget";
            this._LabelTarget.Size = new System.Drawing.Size(42, 13);
            this._LabelTarget.TabIndex = 2;
            this._LabelTarget.Text = "&Target:";
            // 
            // _TextTarget
            // 
            this._TextTarget.BackColor = System.Drawing.SystemColors.Window;
            this._TextTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextTarget.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextTarget.Location = new System.Drawing.Point(30, 142);
            this._TextTarget.Name = "_TextTarget";
            this._TextTarget.Size = new System.Drawing.Size(622, 25);
            this._TextTarget.TabIndex = 2;
            this._TextTarget.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelTargetWarning
            // 
            this._LabelTargetWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTargetWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTargetWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelTargetWarning.Image")));
            this._LabelTargetWarning.Location = new System.Drawing.Point(7, 142);
            this._LabelTargetWarning.Name = "_LabelTargetWarning";
            this._LabelTargetWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelTargetWarning.TabIndex = 2;
            this._ToolTip.SetToolTip(this._LabelTargetWarning, "Target is empty");
            this._LabelTargetWarning.Visible = false;
            // 
            // _LabelNameError
            // 
            this._LabelNameError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNameError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNameError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelNameError.Image")));
            this._LabelNameError.Location = new System.Drawing.Point(7, 43);
            this._LabelNameError.Name = "_LabelNameError";
            this._LabelNameError.Size = new System.Drawing.Size(25, 25);
            this._LabelNameError.TabIndex = 0;
            this._ToolTip.SetToolTip(this._LabelNameError, "Name is empty");
            this._LabelNameError.Visible = false;
            // 
            // _LabelIdError
            // 
            this._LabelIdError.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIdError.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIdError.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIdError.Image")));
            this._LabelIdError.Location = new System.Drawing.Point(7, 89);
            this._LabelIdError.Name = "_LabelIdError";
            this._LabelIdError.Size = new System.Drawing.Size(25, 25);
            this._LabelIdError.TabIndex = 7;
            this._ToolTip.SetToolTip(this._LabelIdError, "ID is empty");
            this._LabelIdError.Visible = false;
            // 
            // _LabelValuesWarning
            // 
            this._LabelValuesWarning.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelValuesWarning.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelValuesWarning.Image = ((System.Drawing.Image)(resources.GetObject("_LabelValuesWarning.Image")));
            this._LabelValuesWarning.Location = new System.Drawing.Point(7, 218);
            this._LabelValuesWarning.Name = "_LabelValuesWarning";
            this._LabelValuesWarning.Size = new System.Drawing.Size(25, 25);
            this._LabelValuesWarning.TabIndex = 4;
            this._ToolTip.SetToolTip(this._LabelValuesWarning, "Values is empty");
            this._LabelValuesWarning.Visible = false;
            // 
            // _TextValues
            // 
            this._TextValues.BackColor = System.Drawing.SystemColors.Window;
            this._TextValues.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextValues.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextValues.Location = new System.Drawing.Point(30, 218);
            this._TextValues.Multiline = true;
            this._TextValues.Name = "_TextValues";
            this._TextValues.Size = new System.Drawing.Size(622, 113);
            this._TextValues.TabIndex = 4;
            this._TextValues.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelValues
            // 
            this._LabelValues.AutoSize = true;
            this._LabelValues.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelValues.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelValues.Location = new System.Drawing.Point(27, 202);
            this._LabelValues.Name = "_LabelValues";
            this._LabelValues.Size = new System.Drawing.Size(43, 13);
            this._LabelValues.TabIndex = 4;
            this._LabelValues.Text = "&Values:";
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(27, 396);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 6;
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.Color.LightYellow;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextDescription.Location = new System.Drawing.Point(30, 412);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(622, 93);
            this._TextDescription.TabIndex = 6;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _CheckClean
            // 
            this._CheckClean.AutoSize = true;
            this._CheckClean.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckClean.Location = new System.Drawing.Point(30, 171);
            this._CheckClean.Name = "_CheckClean";
            this._CheckClean.Size = new System.Drawing.Size(163, 21);
            this._CheckClean.TabIndex = 3;
            this._CheckClean.Text = " &Clean previous version";
            this._CheckClean.UseVisualStyleBackColor = true;
            this._CheckClean.CheckedChanged += new System.EventHandler(this._Changed);
            // 
            // _TextCondition
            // 
            this._TextCondition.BackColor = System.Drawing.Color.MintCream;
            this._TextCondition.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCondition.ForeColor = System.Drawing.SystemColors.WindowText;
            this._TextCondition.Location = new System.Drawing.Point(30, 359);
            this._TextCondition.Name = "_TextCondition";
            this._TextCondition.Size = new System.Drawing.Size(622, 25);
            this._TextCondition.TabIndex = 5;
            this._TextCondition.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelConditionHint
            // 
            this._LabelConditionHint.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelConditionHint.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelConditionHint.Image = global::Robbiblubber.BadWolf.Struct.Resources.hash;
            this._LabelConditionHint.Location = new System.Drawing.Point(9, 359);
            this._LabelConditionHint.Name = "_LabelConditionHint";
            this._LabelConditionHint.Size = new System.Drawing.Size(25, 25);
            this._LabelConditionHint.TabIndex = 5;
            this._LabelConditionHint.Visible = false;
            // 
            // _LabelCondition
            // 
            this._LabelCondition.AutoSize = true;
            this._LabelCondition.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCondition.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCondition.Location = new System.Drawing.Point(27, 343);
            this._LabelCondition.Name = "_LabelCondition";
            this._LabelCondition.Size = new System.Drawing.Size(62, 13);
            this._LabelCondition.TabIndex = 5;
            this._LabelCondition.Text = "&Condition:";
            // 
            // ConfigurationFileControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this._TextCondition);
            this.Controls.Add(this._LabelConditionHint);
            this.Controls.Add(this._LabelCondition);
            this.Controls.Add(this._CheckClean);
            this.Controls.Add(this._TextValues);
            this.Controls.Add(this._LabelValuesWarning);
            this.Controls.Add(this._LabelValues);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._TextTarget);
            this.Controls.Add(this._TextID);
            this.Controls.Add(this._LabelIdError);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelNameError);
            this.Controls.Add(this._LabelTargetWarning);
            this.Controls.Add(this._LabelTarget);
            this.Controls.Add(this._LabelID);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ConfigurationFileControl";
            this.Size = new System.Drawing.Size(688, 505);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelID;
        private System.Windows.Forms.TextBox _TextID;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelTarget;
        private System.Windows.Forms.TextBox _TextTarget;
        private System.Windows.Forms.Label _LabelTargetWarning;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _LabelNameError;
        private System.Windows.Forms.Label _LabelIdError;
        private System.Windows.Forms.TextBox _TextValues;
        private System.Windows.Forms.Label _LabelValuesWarning;
        private System.Windows.Forms.Label _LabelValues;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.CheckBox _CheckClean;
        private System.Windows.Forms.TextBox _TextCondition;
        private System.Windows.Forms.Label _LabelConditionHint;
        private System.Windows.Forms.Label _LabelCondition;
    }
}
