﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>Installer steps implement this interface.</summary>
    public interface IStep: IItem
    {}
}
