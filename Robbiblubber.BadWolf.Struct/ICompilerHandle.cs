﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This interface allows packages and installers to modify the current target directory.</summary>
    public interface ICompilerHandle
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the target directory.</summary>
        /// <param name="dir">Directory.</param>
        void SetTargetDirectory(string dir);
    }
}
