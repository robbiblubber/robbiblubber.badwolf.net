﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Library;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This class provides a basic item view control.</summary>
    public partial class InstallerPackageControl: UserControl, IItemControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        private InstallerPackage _Item;

        /// <summary>Project.</summary>
        private Project _Project;

        /// <summary>Active flag.</summary>
        private bool _Active = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public InstallerPackageControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItemControl                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when the item has changed.</summary>
        public event ItemEventHandler ItemChanged;


        /// <summary>Shows an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="project">Project.</param>
        public void ShowItem(IItem item, Project project)
        {
            _Active = false;
            _Item = (InstallerPackage) item;
            _Project = project;

            _TextName.Text = _Item.Name;
            _TextID.Text = _Item.ID;
            _TextDescription.Text = _Item.Description;

            _CheckVisible.Checked = _Item.Visible;
            _CheckSelectable.Checked = _Item.Selectable;
            _CheckSelected.Checked = _Item.Selected;

            _Active = true;
            Visible = true;
        }


        /// <summary>Gets the control.</summary>
        /// <returns>Control.</returns>
        public Control GetControl()
        {
            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            if(_Active)
            {
                _Item.Visible = _CheckVisible.Checked;
                _Item.Selectable = _CheckSelectable.Checked;
                _Item.Selected = _CheckSelected.Checked;
            }
        }
    }
}
