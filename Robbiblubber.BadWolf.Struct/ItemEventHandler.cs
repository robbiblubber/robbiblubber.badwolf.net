﻿using System;



namespace Robbiblubber.BadWolf.Struct
{
    /// <summary>This delegate provides an event handler for item events.</summary>
    /// <param name="sender">Sending object.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void ItemEventHandler(object sender, ItemEventArgs e);
}
